package ring.collections.impl;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Map.Entry;
import java.util.Objects;

import ring.collections.*;
import ring.func.Equalizer;

public final class HashSet<E> extends AbstractCollection<E> {
	private static final Object value = new Object();
	private final Map<E,Object> data;
	
	public HashSet() { this.data = new HashMap<>(); }
	
	public HashSet(Equalizer<E> eq) { this.data = new HashMap<>(eq); }
	
	public HashSet(Collection<? extends E> c) { this(); addAll(c); }
	
	public HashSet(Collection<? extends E> c, Equalizer<E> eq) { this(eq); addAll(c); }
	
	@Override public Equalizer<E> equalizer() { return data.keyEqualizer(); }
	
	@Override public Enumerator<E> enumerate() { return new HashSetEtr<>(data.enumerate()); }
	
	private static class HashSetEntry<E> implements Entry<E,Object> {
		final E key;
		
		HashSetEntry(E key) { this.key = Objects.requireNonNull(key); }
		
		@Override public E getKey() { return key; }
		
		@Override public Object getValue() { return value; }
		
		@Override public Object setValue(Object value) { throw new UnsupportedOperationException(); }
	}
	
	private static class HashSetEtr<E> implements Enumerator<E> {
		protected final Enumerator<Entry<E,Object>> elems;
		
		HashSetEtr(Enumerator<Entry<E,Object>> elems) { this.elems = Objects.requireNonNull(elems); }
		
		@Override public E curr() { return elems.curr().getKey(); }
		
		@Override public int size() { return elems.size(); }
		
		@Override public void jumpToStart() { elems.jumpToStart(); }
		
		@Override public void jumpToEnd() { elems.jumpToEnd(); }
		
		@Override public boolean next() { return elems.next(); }
		
		@Override public boolean prev() { return elems.prev(); }
		
		@Override public boolean findFirst(E elem) { return elems.findFirst(new KeyEntry<>(elem)); }
		
		@Override public void add(E elem) { elems.add(new HashSetEntry<>(elem)); }
		
		@Override public int addAll(Collection<? extends E> c) { return elems.addAll(c.map(HashSetEntry::new)); }
		
		@Override public E remove() { return elems.remove().getKey(); }
		
		@Override public void flush() { elems.flush(); }
		
		@Override public void close() { elems.close(); }
	}
}
