package ring.collections.impl;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Objects;

import ring.collections.AbstractList;
import ring.collections.Collection;
import ring.collections.Enumerable;
import ring.collections.Enumerator;
import ring.util.IndexException;

public final class ArrayList<E> extends AbstractList<E> {
	private static final int minCapacity = 10;
	private static final Object[] empty = {};
	private Object[] data;
	private int size, modCount;
	
	public ArrayList() { this.data = empty; }
	
	public ArrayList(int initialCapacity) { this(); ensureCapacity(initialCapacity); }
	
	public ArrayList(Collection<? extends E> c) { this(c.size()); addAll(c); }
	
	private ArrayList(Object[] data) { this.data = Objects.requireNonNull(data); }
	
	private Object[] trimmedInternalArray() { trimToSize(); return data; }
	
	@Deprecated
	public static Object[] toArray0(Enumerable<?> elems) {
		var l = new ArrayList<>();
		for(var e : elems) l.add(e);
		return l.trimmedInternalArray();
	}
	
	@Deprecated @SuppressWarnings("unchecked")
	public static <T> T[] toArray0(Enumerable<?> elems, T[] a) {
		var l = new ArrayList<>(a);
		for(var e : elems) l.add(e);
		return (T[])l.trimmedInternalArray();
	}
	
	@Deprecated public static Object[] toArray0(Collection<?> c) { return new ArrayList<>(c).trimmedInternalArray(); }
	
	@Deprecated @SuppressWarnings("unchecked")
	public static <T> T[] toArray0(Collection<?> c, T[] a) { var l = new ArrayList<>(a); l.addAll(c); return (T[])l.trimmedInternalArray(); }
	
	@Override
	public void ensureCapacity(int minCapacity) {
		if(data.length >= minCapacity) return;
		modCount++;
		minCapacity = Math.max(minCapacity,ArrayList.minCapacity);
		var tmp = (Object[])java.lang.reflect.Array.newInstance(data.getClass().getComponentType(),minCapacity);
		System.arraycopy(data,0,tmp,0,size);
		Arrays.fill(data,0,size,null);
		data = tmp;
	}
	
	private void grow(int growBy) {
		if(size+growBy > data.length) ensureCapacity(size+size/2+growBy);
	}
	
	@Override
	public void trimToSize() {
		if(data.length == size) return;
		modCount++;
		if(size == 0) {
			Arrays.fill(data,0,size,null);
			data = empty;
		}
		else {
			var tmp = Arrays.copyOf(data,size);
			Arrays.fill(data,0,size,null);
			data = tmp;
		}
	}
	
	@Override public Enumerator<E> enumerate() { return new ArrayListEtr<>(this); }
	
	private static class ArrayListEtr<E> extends AbstractGapArrayEtr<E> {
		final ArrayList<E> owner; int modCount;
		
		ArrayListEtr(ArrayList<E> owner) { this.owner = Objects.requireNonNull(owner); resetModCnt(); flush(); }
		
		void checkModCnt() {
			if(modCount != owner.modCount) throw new ConcurrentModificationException();
		}
		
		void resetModCnt() { modCount = owner.modCount; }
		
		void incModCnt() { owner.modCount++; modCount++; }
		
		@SuppressWarnings("unchecked")
		@Override public E curr() { checkModCnt(); return (E)owner.data[elemIndex0()]; }
		
		@Override public int size() { return owner.size; }
		
		@SuppressWarnings("unchecked")
		@Override public E set(E elem) { checkModCnt(); int i = elemIndex0(); E ret = (E)owner.data[i]; owner.data[i] = elem; return ret; }
		
		private void checkAddable() {
			if(index() < size()) return;
			if(index() == size()) throw new IndexException(-1,size(),index(),true,false);
			throw new ConcurrentModificationException();
		}
		
		@Override
		public void add(E elem) {
			checkAddable(); flush(); owner.grow(1); resetModCnt(); jumpTo(index()+1);
			System.arraycopy(owner.data,index(),owner.data,index()+1,owner.size-index());
			owner.data[index()] = elem; owner.size++; incModCnt();
		}
		
		@Override
		public int addAll(Collection<? extends E> c) {
			var s = c.size();
			checkAddable(); flush(); owner.grow(s); resetModCnt(); jumpTo(index()+1);
			System.arraycopy(owner.data,index(),owner.data,index()+s,owner.size-index());
			var elems = c.enumerate();
			for(int i = 0; i < s; i++) {
				if(elems.next()) owner.data[index()+i] = elems.curr();
				else { gapFrom = index()+i; gapLen = s-i; s = i; } //less elements than expected
			}
			owner.size += s; jumpTo(index()+s-1); incModCnt();
			while(elems.next()) { add(elems.curr()); s++; } //more elements than expected
			return s;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public E remove() {
			checkModCnt(); int i = elemIndex0();
			if(gapLen == 0) { gapFrom = i; gapLen = 1; }
			else if(i < gapFrom) {
				int len = gapFrom-i-1;
				System.arraycopy(owner.data,i+1,owner.data,gapTo()-len,len);
				gapFrom = i; gapLen++;
			}
			else {
				i += gapLen; int len = i-gapTo();
				System.arraycopy(owner.data,gapTo(),owner.data,gapFrom,len);
				gapFrom += len; gapLen++;
			}
			E ret = (E)owner.data[i]; owner.data[i] = null;
			owner.size--; incModCnt(); return ret;
		}
		
		@Override
		public void flush() {
			checkModCnt();
			if(gapLen > 0) {
				System.arraycopy(owner.data,gapTo(),owner.data,gapFrom,owner.size+gapLen-gapTo());
				if(index() >= gapTo()) jumpTo(index()-gapLen);
				incModCnt();
			}
			gapFrom = Integer.MAX_VALUE; gapLen = 0;
		}
	}
}
