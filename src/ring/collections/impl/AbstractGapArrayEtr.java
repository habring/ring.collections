package ring.collections.impl;

import ring.collections.AbstractIndexedEnumerator;
import ring.util.ArrayUtil;

abstract class AbstractGapArrayEtr<E> extends AbstractIndexedEnumerator<E> {
	int gapFrom, gapLen;
	
	int gapTo() { return gapFrom+gapLen; }
	
	int elemIndex0() { return ArrayUtil.checkIndex(size(),index0()); }
	
	int index0() { return index0(index()); }
	
	int index0(int i) { return i < gapFrom ? i : i+gapLen; }
}
