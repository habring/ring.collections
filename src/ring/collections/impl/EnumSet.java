package ring.collections.impl;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.NoSuchElementException;
import java.util.Objects;

import ring.collections.AbstractCollection;
import ring.collections.Collection;
import ring.collections.Enumerator;
import ring.collections.List;
import ring.func.Equalizer;

public final class EnumSet<E extends Enum<E>> extends AbstractCollection<E> {
	private static final ErrorMessages errors = new ErrorMessages();
	private final Class<E> enumCls;
	private final List<E> all;
	@SuppressWarnings("TODO use unsigned long + allow Long.SIZE amount of values")
	private long value;
	
	public EnumSet(Class<E> enumCls) { this(enumCls,Collection.empty()); }
	
	public EnumSet(EnumSet<E> s) { this(s.enumCls,s); }
	
	public EnumSet(Class<E> enumCls, Collection<E> c) {
		if(c instanceof EnumSet<E> s) {
			if(enumCls != s.enumCls) throw new IllegalArgumentException("enum type mismatch");
			this.enumCls = s.enumCls; this.all = s.all; this.value = s.value;
		}
		else {
			if(!enumCls.isEnum()) throw new IllegalArgumentException(enumCls+" is not an enum type");
			this.enumCls = enumCls; this.all = List.of(enumCls.getEnumConstants());
			if(all.size() >= Long.SIZE) throw new IllegalArgumentException("enum has more than "+Long.SIZE+" values");
			addAll(c);
		}
	}
	
	@Override public Equalizer<E> equalizer() { return Equalizer.natural(); }
	
	@Override public Enumerator<E> enumerate() { return new EnumSetEtr<>(this); }
	
	@Override public int hashCode() { return Long.hashCode(value); }
	
	@SuppressWarnings("rawtypes")
	@Override public boolean equals(Object obj) { return obj instanceof EnumSet s ? enumCls == s.enumCls && value == s.value : super.equals(obj); }
	
	private static class EnumSetEtr<E extends Enum<E>> implements Enumerator<E> {
		private final EnumSet<E> owner;
		private int i;
		
		EnumSetEtr(EnumSet<E> owner) { this.owner = Objects.requireNonNull(owner); jumpToStart(); }
		
		boolean isPresent(int i) { return (owner.value & (1L<<i)) != 0; }
		
		@Override
		public E curr() {
			if(!isPresent(i)) throw new NoSuchElementException();
			return owner.all.get(i);
		}
		
		@Override public int size() { return Long.bitCount(owner.value); }
		
		@Override public void jumpToStart() { i = -1; }
		
		@Override public void jumpToEnd() { i = owner.all.size(); }
		
		@Override
		public boolean next() {
			while(i < owner.all.size()) {
				i++;
				if(isPresent(i)) return true;
			}
			return false;
		}
		
		@Override
		public boolean prev() {
			while(i >= 0) {
				i--;
				if(i >= 0 && isPresent(i)) return true;
			}
			return false;
		}
		
		@Override
		public boolean findFirst(E elem) {
			if(elem == null) { jumpToEnd(); return false; }
			int j = elem.ordinal(); var p = j > i && isPresent(j);
			if(p) i = j;
			return p;
		}
		
		@Override
		public void add(E elem) {
			int j = elem.ordinal();
			if(isPresent(j)) throw new IllegalArgumentException(elem+" already exists");
			owner.value |= 1L << j; i = j;
		}
		
		@Override
		public int addAll(Collection<? extends E> c) {
			if(c instanceof EnumSet<? extends E> s && owner.enumCls == s.enumCls && (owner.value&s.value) == 0) {
				owner.value |= s.value; i = Long.SIZE-1-Long.numberOfLeadingZeros(s.value); return s.size();
			}
			else return Enumerator.super.addAll(c);
		}
		
		@Override public E remove() { E ret = curr(); owner.value &= ~(1L << i); jumpToStart(); return ret; }
	}
}
