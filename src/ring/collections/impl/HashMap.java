package ring.collections.impl;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Objects;

import ring.collections.AbstractMap;
import ring.collections.Collection;
import ring.collections.Enumerator;
import ring.collections.Map;
import ring.func.Equalizer;

public final class HashMap<K,V> extends AbstractMap<K,V> {
	private static final ErrorMessages errors = new ErrorMessages();
	private static final double loadFactor = 0.75, loadFactorTolerance = 0.3;
	private static final int minNumBuckets = 0x10;
	private final Equalizer<K> eq;
	private Node<K,V>[] buckets;
	private int size, modCount;
	
	private static class Node<K,V> implements Entry<K,V> {
		final K key; V value;
		
		Node(Node<K,V> n) { this(n.key,n.value); }
		
		Node(K key, V value) { this.key = Objects.requireNonNull(key); setValue(value); }
		
		@Override public K getKey() { return key; }
		
		@Override public V getValue() { return value; }
		
		@Override public V setValue(V value) { var ret = this.value; this.value = Objects.requireNonNull(value); return ret; }
		
		Node2<K,V> cast() { return null; }
		
		int size() {
			var ret = 1; var n = cast();
			if(n != null) { for(n = n.next; n != null; n = n.next) ret++; }
			return ret;
		}
	}
	
	private static class Node2<K,V> extends Node<K,V> {
		Node2<K,V> next;
		
		Node2(Node<K,V> n) { super(n); }
		
		Node2(K key, V value) { super(key,value); }
		
		@Override Node2<K,V> cast() { return this; }
	}
	
	public HashMap() { this((Equalizer<K>)null); }
	
	public HashMap(Equalizer<K> eq) {
		this.eq = eq != null ? eq : Equalizer.natural(); this.buckets = newEmptyBuckets(minNumBuckets);
	}
	
	public HashMap(Map<K,V> m) { this(m,m.keyEqualizer()); }
	
	public HashMap(Collection<? extends Entry<K,V>> c, Equalizer<K> eq) {
		this(eq); addAll(c);
	}
	
	@Deprecated
	int[] bucketStatistics() {
		var ret = new int[1];
		for(int i = 0; i < buckets.length; i++) {
			var n = buckets[i] == null ? 0 : buckets[i].size();
			if(n >= ret.length) ret = Arrays.copyOf(ret,n+1);
			ret[n]++;
		}
		return ret;
	}
	
	@Override public Equalizer<K> keyEqualizer() { return eq; }
	
	@Override public int size() { return size; }
	
	@Override public Enumerator<Entry<K,V>> enumerate() { return new HashMapEtr<>(this); }
	
	private void optimize(int newSize) {
		var lf = newSize / (double)buckets.length;
		boolean low = lf < loadFactor-loadFactorTolerance, high = lf > loadFactor+loadFactorTolerance;
		if(!low && !high) return;
		var n = Math.max(low?buckets.length/2:buckets.length*2,minNumBuckets);
		if(n == buckets.length || Math.abs(lf-loadFactor) <= Math.abs(newSize/(double)n-loadFactor)) return;
		var oldBuckets = buckets; buckets = newEmptyBuckets(n);
		try(var tmp = new HashMapEtr<>(this)) { copyToAndClearSrc(oldBuckets,tmp,buckets); }
		modCount++;
	}
	
	@SuppressWarnings("unchecked")
	private static <K,V> Node<K,V>[] newEmptyBuckets(int numBuckets) { return new Node[numBuckets]; }
	
	private static <K,V> void copyToAndClearSrc(Node<K,V>[] src, HashMapEtr<K,V> etr, Node<K,V>[] dest) {
		for(int i = 0; i < src.length; i++) {
			if(src[i] == null) continue;
			var n = src[i].cast();
			if(n == null) etr.add0(dest,src[i]);
			while(n != null) { etr.add0(dest,n); var n2 = n; n = n.next; n2.next = null; }
			src[i] = null;
		}
	}
	
	private int bucket(int numBuckets, K key) {
		int i = eq.hashCode(Objects.requireNonNull(key));
		return Math.abs(i ^ (i>>>16) ^ (numBuckets-1)) % numBuckets;
	}
	
	private Node<K,V> find(Node<K,V>[] buckets, int i, K key, Node2<K,V> prevOfStartNode, boolean reorder) {
		if(buckets[i] == null) return null;
		var bucket = buckets[i].cast();
		if(bucket == null) return eq.equals(buckets[i].key,key) ? buckets[i] : null;
		var n = prevOfStartNode != null ? prevOfStartNode.next : bucket;
		for(var p = prevOfStartNode; n != null; p = n, n = n.next) {
			if(!eq.equals(n.key,key)) continue;
//			move elements that are more frequently searched to the front
			if(p != null && reorder) { modCount++; p.next = n.next; n.next = bucket; buckets[i] = n; }
			return n;
		}
		return null;
	}
	
	private static class HashMapEtr<K,V> implements Enumerator<Entry<K,V>> {
		final HashMap<K,V> owner;
		int modCount;
		int i;
		Node<K,V> n; Node2<K,V> n2;
		
		HashMapEtr(HashMap<K,V> owner) { this.owner = Objects.requireNonNull(owner); resetModCnt(); jumpToStart(); }
		
		void checkModCnt() {
			if(modCount != owner.modCount) throw new ConcurrentModificationException();
		}
		
		void resetModCnt() { modCount = owner.modCount; }
		
		void incModCnt() { owner.modCount++; resetModCnt(); }
		
		@Override
		public Entry<K,V> curr() {
			if(n != null) return n;
			throw new NoSuchElementException();
		}
		
		@Override public int size() { return owner.size; }
		
		@Override public void jumpToStart() { checkModCnt(); i = 0; n(null); }
		
		@Override public void jumpToEnd() { checkModCnt(); i = owner.buckets.length; n(null); }
		
		Node<K,V> n(Node<K,V> n) { this.n = n; n2 = n.cast(); return n; }
		
		Node2<K,V> n(Node2<K,V> n) { this.n = n; n2 = n; return n; }
		
		Node<K,V> copy(Entry<K,V> e) { return new Node<>(e.getKey(),e.getValue()); }
		
		Node2<K,V> copy2(Entry<K,V> e) { return new Node2<>(e.getKey(),e.getValue()); }
		
		@Override
		public boolean findFirst(Entry<K,V> elem) {
			checkModCnt(); K key = elem.getKey(); V value = elem.getValue();
			var x = owner.bucket(owner.buckets.length,key);
			Node<K,V> y;
			if(x <= i) {
				if(x < i) { jumpToEnd(); return false; }
				y = owner.find(owner.buckets,x,key,n2,true);
			}
			else y = owner.find(owner.buckets,x,key,null,true);
			resetModCnt();
			if(y == null || (value != null && !Objects.equals(y.value,value))) {
				jumpToEnd(); return false;
			}
			i = x; n(y); return true;
		}
		
		@Override
		public boolean next() {
			checkModCnt();
			if(n2 != null) {
				n(n2.next);
				if(n != null) return true;
				i++;
			}
			else if(n != null) { n(null); i++; }
			for(; i < owner.buckets.length; i++) {
				if(owner.buckets[i] == null) continue;
				n(owner.buckets[i]); return true;
			}
			jumpToEnd(); return false;
		}
		
		@Override
		public boolean prev() {
			checkModCnt();
			if(n != null && n != owner.buckets[i]) {
				for(var x = owner.buckets[i].cast(); x != null; x = x.next) {
					if(x.next != n) continue;
					n(x); return true;
				}
				throw new Error(errors.unreachable);
			}
			for(i--; i >= 0; i--) {
				if(owner.buckets[i] == null) continue;
				var x = owner.buckets[i].cast();
				if(x == null) { n(owner.buckets[i]); return true; }
				while(x.next != null) x = x.next;
				n(x); return true;
			}
			jumpToStart(); return false;
		}
		
		@Override
		public Entry<K,V> set(Entry<K,V> elem) {
			if(!owner.eq.equals(elem.getKey(),curr().getKey())) throw new IllegalArgumentException();
			checkModCnt(); var ret = new SimpleImmutableEntry<>(n);
			n.setValue(elem.getValue()); return ret;
		}
		
		@Override
		public void add(Entry<K,V> elem) {
			checkModCnt(); owner.optimize(owner.size+1);
			add0(owner.buckets,elem); owner.size++; incModCnt();
		}
		
		@Override
		public int addAll(Collection<? extends Entry<K,V>> c) {
			checkModCnt(); owner.optimize(owner.size+c.size());
			int num = 0;
			for(var e : c) { add0(owner.buckets,e); num++; }
			owner.size += num; incModCnt(); return num;
		}
		
		void add0(Node<K,V>[] buckets, Entry<K,V> elem) {
			K key = elem.getKey();
			var x = owner.bucket(buckets.length,key);
			var y = owner.find(buckets,x,key,null,false);
			if(y != null) throw new IllegalArgumentException(key+" already exists");
			i = x;
			if(buckets[i] == null) buckets[i] = n(copy(elem));
			else {
				var bucket = buckets[i].cast();
				if(bucket == null) buckets[i] = bucket = new Node2<>(buckets[i]);
				n(copy2(elem)); n2.next = bucket; buckets[i] = n;
			}
		}
		
		@Override
		public Entry<K,V> remove() {
			checkModCnt();
			var e = curr();
			if(n == owner.buckets[i]) {
				if(n2 == null) owner.buckets[i] = n(null);
				else {
					owner.buckets[i] = n(n2.next);
					if(n2 != null && n2.next == null) owner.buckets[i] = n(new Node<>(owner.buckets[i]));
				}
				if(n != owner.buckets[i]) throw new Error(errors.unreachable);
			}
			else {
				prev();
				if(n2.next != e) throw new Error(errors.unreachable);
				n2.next = n2.next.next;
			}
			owner.size--; owner.optimize(owner.size); incModCnt(); jumpToStart(); return e;
		}
	}
}
