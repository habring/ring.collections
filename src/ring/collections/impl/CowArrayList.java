package ring.collections.impl;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.Arrays;
import java.util.Objects;

import ring.collections.AbstractList;
import ring.collections.Collection;
import ring.collections.Enumerator;
import ring.collections.List;
import ring.util.ArrayUtil;

/**A concurrent lockless copy on write implementation that uses optimistic concurrency.<br>
 * Caution: experimental, may be removed in future versions;*/
public final class CowArrayList<E> extends AbstractList<E> {
	private static final ErrorMessages errors = new ErrorMessages();
	private static final VarHandle dataHandle;
	private static final Object[] empty = {};
	private volatile Object[] data = empty;
	
	static {
		try {
			var lookup = MethodHandles.lookup();
			dataHandle = lookup.findVarHandle(CowArrayList.class,"data",Object[].class);
		} catch (ReflectiveOperationException e) { throw new Error(e); }
	}
	
	public CowArrayList() {}
	
	public CowArrayList(Collection<? extends E> c) { addAll(c); }
	
	private boolean setData(Object[] oldData, Object[] newData) { return dataHandle.compareAndSet(this,oldData,newData); }
	
	@Override public Enumerator<E> enumerate() { return new CowArrayListEtr<>(this); }
	
	private static class CowArrayListEtr<E> extends AbstractGapArrayEtr<E> {
		final CowArrayList<E> owner;
		Object[] data;
		
		CowArrayListEtr(CowArrayList<E> owner) {
			this.owner = Objects.requireNonNull(owner); this.data = owner.data; flush();
		}
		
		@SuppressWarnings("unchecked")
		@Override public E curr() { return (E)data[elemIndex0()]; }
		
		@Override public int size() { return data.length-gapLen; }
		
		private boolean setData(Object[] data) {
			if(!owner.setData(this.data,data)) return false;
			this.data = data; return true;
		}
		
		boolean refreshData() {
			if(gapLen > 0) throw new Error(errors.unreachable);
			var data = this.data; int i = index();
			this.data = owner.data;
			if(i < data.length) {
				if(i < this.data.length && data[i] == this.data[i]) return true;
				jumpToEnd(); return false;
			}
			else { jumpToEnd(); return true; }
		}
		
		@Override
		public E set(E elem) {
			E ret = curr();
			flush();
			while(true) {
				if(!refreshData()) {
					jumpToStart();
					if(!findFirst(ret)) break;
				}
				if(index() >= data.length) break;
				var tmp = Arrays.copyOf(data,data.length);
				tmp[index()] = elem;
				if(setData(tmp)) break;
			}
			return ret;
		}
		
		@Override public void add(E elem) { addAll(List.of(elem)); }
		
		@Override
		public int addAll(Collection<? extends E> c) {
			int i = ArrayUtil.checkIndex(size(),index())+1;
			flush();
			int ret;
			while(true) {
				if(!refreshData()) jumpTo(i=Math.min(i,data.length));
				var elems = c.enumerate();
				ret = elems.size();
				var tmp = new Object[data.length+ret];
				System.arraycopy(data,0,tmp,0,i);
				System.arraycopy(data,i,tmp,i+ret,data.length-i);
				var sizeMismatch = false;
				for(int j = 0; j < ret; j++) {
					if(!elems.next()) { sizeMismatch = true; break; }
					tmp[i+j] = elems.curr();
				}
				if(sizeMismatch || elems.next()) continue;
				if(setData(tmp)) break;
			}
			jumpTo(i+ret-1); return ret;
		}
		
		@Override
		public E remove() {
			E ret = curr();
			var flushed = false;
			while(true) {
				if(gapLen == 0) { gapFrom = index(); gapLen = 1; break; }
				else if(index() == gapTo()) { gapLen++; break; }
				else if(index() == gapFrom-1) { gapFrom--; gapLen++; break; }
				else if(flushed) throw new Error(errors.unreachable);
				else { flush(); flushed = true; }
			}
			return ret;
		}
		
		@SuppressWarnings("TODO remove() work on its own (no flush() at all)")
		@Override
		public void flush() {
			var data = this.data; int from = gapFrom, to = gapTo();
			gapFrom = Integer.MAX_VALUE; gapLen = 0;
			if(from == to) return;
			while(true) { //delete [from,to[
				refreshData();
				if(to <= this.data.length && this.data[from] == data[from] && this.data[to-1] == data[to-1]) {
					var tmp = new Object[this.data.length-(to-from)];
					System.arraycopy(this.data,0,tmp,0,from);
					System.arraycopy(this.data,to,tmp,from,this.data.length-to);
					if(setData(tmp)) break;
				}
				else {
					throw new UnsupportedOperationException();
				}
			}
		}
	}
}
