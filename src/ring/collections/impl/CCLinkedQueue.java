package ring.collections.impl;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.NoSuchElementException;
import java.util.Objects;

import ring.collections.AbstractCollection;
import ring.collections.Collection;
import ring.collections.Enumerator;
import ring.collections.Queue;

/**A concurrent lockless linked queue implementation that uses optimistic concurrency.*/
//Simple, Fast, and Practical Non-Blocking and BlockingConcurrent Queue Algorithms (Maged M. Michael, Michael L. Scott)
//https://cs.rochester.edu/u/scott/papers/1996_PODC_queues.pdf
public final class CCLinkedQueue<E> extends AbstractCollection<E> implements Queue<E> {
	private static final VarHandle headHandle, tailHandle, sizeHandle, nextHandle, elemHandle;
	private volatile Node<E> head, tail;
	private volatile int size = 0;
	
	static { //implementation was adapted from the AtomicReference class
		try {
			var lookup = MethodHandles.lookup();
			headHandle = lookup.findVarHandle(CCLinkedQueue.class,"head",Node.class);
			tailHandle = lookup.findVarHandle(CCLinkedQueue.class,"tail",Node.class);
			sizeHandle = lookup.findVarHandle(CCLinkedQueue.class,"size",int.class);
			nextHandle = lookup.findVarHandle(Node.class,"next",Node.class);
			elemHandle = lookup.findVarHandle(Node.class,"elem",Object.class);
		} catch (ReflectiveOperationException e) { throw new Error(e); }
	}
	
	private static class Node<E> {
		volatile Node<E> next; volatile E elem;
		
		boolean setNext(Node<E> expected, Node<E> newValue) { return nextHandle.compareAndSet(this,expected,newValue); }
		
		E setElem(E elem) { return (E)elemHandle.getAndSet(this,elem); }
	}
	
	public CCLinkedQueue() { head = tail = new Node<>(); }
	
	public CCLinkedQueue(Collection<? extends E> c) { this(); addAll(c); }
	
	private boolean setHead(Node<E> expected, Node<E> newValue) { return headHandle.compareAndSet(this,expected,newValue); }
	
	private boolean setTail(Node<E> expected, Node<E> newValue) { return tailHandle.compareAndSet(this,expected,newValue); }
	
	private void addToSize(int diff) { sizeHandle.getAndAdd(this,diff); }
	
	@Override
	public E removeHead() {
		while(true) {
			var head = this.head; var tail = this.tail; var next = head.next;
			if(head != this.head) continue;
			if(head == tail) {
				if(next == null) return null;
				setTail(tail,next); continue;
			}
			var ret = next.elem;
			if(setHead(head,next)) { next.elem = null; addToSize(-1); return ret; }
		}
	}
	
	@Override public Enumerator<E> enumerate() { return new CCQueueEtr<>(this); }
	
	private static class CCQueueEtr<E> implements Enumerator<E> {
		private final CCLinkedQueue<E> owner;
		private Node<E> curr;
		
		CCQueueEtr(CCLinkedQueue<E> owner) { this.owner = Objects.requireNonNull(owner); jumpToStart(); }
		
		private void checkExists() {
			if(curr == null || curr == owner.head) throw new NoSuchElementException();
		}
		
		@Override public E curr() { checkExists(); return curr.elem; }
		
		@Override public int size() { return owner.size; }
		
		@Override public void jumpToStart() { curr = owner.head; }
		
		@Override public void jumpToEnd() { curr = null; }
		
		@Override public boolean next() { curr = curr.next; return curr != null; }
		
		@Override public E set(E elem) { checkExists(); return curr.setElem(elem); }
		
		@Override
		public void add(E elem) {
			var n = new Node<E>(); n.elem = Objects.requireNonNull(elem);
			owner.addToSize(1); //doing this first prevents size from becoming negative
			while(true) {
				var tail = owner.tail; var next = tail.next;
				if(tail != owner.tail) continue;
				if(next != null) { owner.setTail(tail,next); continue; }
				if(tail.setNext(null,n)) { owner.setTail(tail,n); break; }
			}
			curr = n;
		}
	}
}
