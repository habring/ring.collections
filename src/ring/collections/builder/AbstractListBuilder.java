package ring.collections.builder;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Comparator;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import ring.collections.Collection;
import ring.collections.List;

abstract class AbstractListBuilder<T,E> extends AbstractCollectionBuilder<T,List<E>,E> {
	AbstractListBuilder(List<E> l) { super(l); }
	
	public List<E> getList() { return get(); }
	
	public T setList(List<E> l) { return set(l); }
	
	public E get(int i) { return get().get(i); }
	
	public T set(int i, E elem) { get().set(i,elem); return returnValue(); }
	
	public T add(int i, E elem) { get().add(i,elem); return returnValue(); }
	
	public T addAll(int i, Collection<? extends E> c) { get().addAll(i,c); return returnValue(); }
	
	public T removeAt(int i) { get().removeAt(i); return returnValue(); }
	
	public T removeRange(int from, int to) { get().removeRange(from,to); return returnValue(); }
	
	public T retainRange(int from, int to) { get().retainRange(from,to); return returnValue(); }
	
	public T setAll(IntFunction<? extends E> f) { get().setAll(f); return returnValue(); }
	
	public T trimStart(Predicate<? super E> p) { get().trimStart(p); return returnValue(); }
	
	public T trimEnd(Predicate<? super E> p) { get().trimEnd(p); return returnValue(); }
	
	public T trim(Predicate<? super E> p) { get().trim(p); return returnValue(); }
	
	public T sort(Comparator<? super E> cmp) { get().sort(cmp); return returnValue(); }
}
