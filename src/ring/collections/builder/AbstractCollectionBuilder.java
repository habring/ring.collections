package ring.collections.builder;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import ring.collections.Collection;
import ring.func.Equalizer;

abstract class AbstractCollectionBuilder<T,C extends Collection<E>,E> {
	private C data;
	
	AbstractCollectionBuilder(C data) { set(data); }
	
	abstract T returnValue();
	
	C get() { return data; }
	
	T set(C data) { this.data = Objects.requireNonNull(data); return returnValue(); }
	
	public int size() { return get().size(); }
	
	public Equalizer<E> equalizer() { return get().equalizer(); }
	
	public T ensureCapacity(int minCapacity) { get().ensureCapacity(minCapacity); return returnValue(); }
	
	public T trimToSize() { get().trimToSize(); return returnValue(); }
	
	public T replaceAll(Function<? super E, ? extends E> f) { get().replaceAll(f); return returnValue(); }
	
	public T add(E elem) { get().add(elem); return returnValue(); }
	
	public T addIfAbsent(E elem) { get().addIfAbsent(elem); return returnValue(); }
	
	public T addAll(Collection<? extends E> c) { get().addAll(c); return returnValue(); }
	
	public T remove(E elem) { get().remove(elem); return returnValue(); }
	
	public T removeIfPresent(E elem) { get().removeIfPresent(elem); return returnValue(); }
	
	public T removeAll(E elem) { get().removeAll(elem); return returnValue(); }
	
	public T removeAllIn(Collection<? super E> c) { get().removeAllIn(c); return returnValue(); }
	
	public T retainAllIn(Collection<? super E> c) { get().retainAllIn(c); return returnValue(); }
	
	public T removeIf(Predicate<? super E> p) { get().removeIf(p); return returnValue(); }
	
	public T clear() { get().clear(); return returnValue(); }
	
	@Override public int hashCode() { return get().hashCode(); }
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null || getClass() != obj.getClass()) return false;
		return get().equals(((AbstractCollectionBuilder<?,?,?>)obj).get());
	}
	
	@Override public String toString() { return get().toString(); }
}
