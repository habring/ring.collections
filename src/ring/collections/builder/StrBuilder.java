package ring.collections.builder;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import ring.collections.List;
import ring.collections.impl.ArrayList;

public final class StrBuilder extends AbstractListBuilder<StrBuilder,Character> {
	public StrBuilder() { this(new ArrayList<>()); }
	
	public StrBuilder(List<Character> l) { super(l); }
	
	@Override StrBuilder returnValue() { return this; }
	
	@Override
	public String toString() {
//		unfortunately, there is no way in java to convert this to a string without copying it twice
		var tmp = new char[size()];
		for(int i = 0; i < tmp.length; i++) tmp[i] = get().get(i);
		return String.valueOf(tmp);
	}
	
	public static String toString(List<Character> l) { return new StrBuilder(l).toString(); }
}
