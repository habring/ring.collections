package ring.collections.builder;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Map.Entry;
import java.util.function.BiFunction;

import ring.collections.Map;
import ring.func.Equalizer;

abstract class AbstractMapBuilder<T,K,V> extends AbstractCollectionBuilder<T,Map<K,V>,Entry<K,V>> {
	AbstractMapBuilder(Map<K,V> data) { super(data); }
	
	public Equalizer<K> keyEqualizer() { return get().keyEqualizer(); }
	
	public Map<K,V> getMap() { return get(); }
	
	public T setMap(Map<K,V> m) { return set(m); }
	
	public V get(K key) { return get().get(key); }
	
	public V get(K key, V defaultValue) { return get().get(key,defaultValue); }
	
	public T set(K key, V value) { get().set(key,value); return returnValue(); }
	
	public T add(K key, V value) { get().add(key,value); return returnValue(); }
	
	public T addIfAbsent(K key, V value) { get().addIfAbsent(key,value); return returnValue(); }
	
	public T setOrAdd(K key, V value) { get().setOrAdd(key,value); return returnValue(); }
	
	public T removeKey(K key) { get().removeKey(key); return returnValue(); }
	
	public T removeKeyIfPresent(K key) { get().removeKeyIfPresent(key); return returnValue(); }
	
	public T compute(K key, BiFunction<? super K,? super V,? extends V> f) { get().compute(key,f); return returnValue(); }
	
	public T replaceAll(BiFunction<? super K,? super V,? extends V> f) { get().replaceAll(f); return returnValue(); }
}
