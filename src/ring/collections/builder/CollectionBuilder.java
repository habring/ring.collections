package ring.collections.builder;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import ring.collections.Collection;
import ring.collections.impl.HashSet;

public final class CollectionBuilder<E> extends AbstractCollectionBuilder<CollectionBuilder<E>,Collection<E>,E> {
	public CollectionBuilder() { this(new HashSet<>()); }
	
	public CollectionBuilder(Collection<E> data) { super(data); }

	@Override CollectionBuilder<E> returnValue() { return this; }
	
	public Collection<E> getCollection() { return get(); }
	
	public CollectionBuilder<E> setCollection(Collection<E> c) { return set(c); }
}
