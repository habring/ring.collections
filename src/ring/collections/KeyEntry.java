package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Map.Entry;
import java.util.Objects;

public final class KeyEntry<K,V> implements Entry<K,V> {
	final K key;
	
	public KeyEntry(K key) { this.key = Objects.requireNonNull(key); }
	
	@Override public K getKey() { return key; }
	
	@Override public V getValue() { return null; }
	
	@Override public V setValue(V value) { throw new UnsupportedOperationException(); }
	
	@Override public int hashCode() { return Objects.hashCode(key); }
	
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Entry e)) return false;
		return Objects.equals(key,e.getKey()) && e.getValue() == null;
	}
	
	@Override public String toString() { return key + "=" + null; }
}
