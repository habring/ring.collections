package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import ring.collections.HelperEtr.ConcatEtr;
import ring.collections.HelperEtr.EmptyEtr;
import ring.collections.HelperEtr.ZippedBase;
import ring.collections.HelperEtr.ZippedEtr;

class HelperColl {
	private static abstract class ProxyEnumerable1<E> implements Enumerable<E> {
		final Enumerable<E> elems;
		
		ProxyEnumerable1(Enumerable<E> elems) { this.elems = Objects.requireNonNull(elems); }
		
		@Override public int hashCode() { return elems.hashCode(); }
		
		@Override public boolean equals(Object obj) { return elems.equals(obj); }
		
		@Override public String toString() { return elems.toString(); }
	}
	
	static class EmptyList<E> extends AbstractList<E> {
		@SuppressWarnings("rawtypes")
		private static final EmptyList instance = new EmptyList<>();
		
		@SuppressWarnings("unchecked")
		static <E> EmptyList<E> get() { return instance; }
		
		private EmptyList() {}
		
		@Override public Enumerator<E> enumerate() { return EmptyEtr.get(); }
		
		@Override public List<E> immutable() { return this; }
	}
	
	static class ImmEnumerable<E> extends ProxyEnumerable1<E> {
		ImmEnumerable(Enumerable<E> elems) { super(elems); }
		
		@Override public Enumerator<E> enumerate() { return elems.enumerate().immutable(); }
		
		@Override public Enumerable<E> immutable() { return this; }
	}
	
	static class ImmColl<E> extends ImmEnumerable<E> implements Collection<E> {
		ImmColl(Collection<E> elems) { super(elems); }
		
		@Override public Collection<E> immutable() { return this; }
	}
	
	static class ImmList<E> extends ImmColl<E> implements List<E> {
		ImmList(List<E> elems) { super(elems); }
		
		@Override public List<E> immutable() { return this; }
	}
	
	static class InvEnumerable<E> extends ProxyEnumerable1<E> {
		InvEnumerable(Enumerable<E> elems) { super(elems); }
		
		@Override public Enumerator<E> enumerate() { var ret = elems.enumerate().inverse(); ret.jumpToStart(); return ret; }
		
		@Override public Enumerable<E> inverse() { return elems; }
	}
	
	static class InvColl<E> extends InvEnumerable<E> implements Collection<E> {
		InvColl(Collection<E> elems) { super(elems); }
		
		@Override public Collection<E> inverse() { return (Collection<E>)elems; }
	}
	
	static class InvList<E> extends InvColl<E> implements List<E> {
		InvList(Collection<E> elems) { super(elems); }
		
		@Override public List<E> inverse() { return (List<E>)elems; }
	}
	
	static class ConcatEnumerable<E> implements Enumerable<E> {
		final Enumerable<? extends Enumerable<E>> elems;
		
		ConcatEnumerable(Enumerable<? extends Enumerable<E>> elems) { this.elems = Objects.requireNonNull(elems); }
		
		@Override public Enumerator<E> enumerate() { return new ConcatEtr<>(elems.enumerate()); }
	}
	
	static class ZippedEnumerable<R,A,B> extends ZippedBase<R,A,B,Enumerable<A>,Enumerable<B>> implements Enumerable<R> {
		ZippedEnumerable(Enumerable<A> first, Enumerable<B> second, BiFunction<A,B,R> combine, Function<A,R> onlyFirst, Function<B,R> onlySecond) {
			super(first,second,combine,onlyFirst,onlySecond);
		}
		
		@Override public Enumerator<R> enumerate() { return new ZippedEtr<>(first.enumerate(),second.enumerate(),combine,onlyFirst,onlySecond); }
	}
	
	static class ZippedColl<R,A,B> extends ZippedEnumerable<R,A,B> implements Collection<R> {
		ZippedColl(Collection<A> first, Collection<B> second, BiFunction<A,B,R> combine, Function<A,R> onlyFirst, Function<B,R> onlySecond) {
			super(first,second,combine,onlyFirst,onlySecond);
		}
	}
	
	static class ZippedList<R,A,B> extends ZippedColl<R,A,B> implements List<R> {
		ZippedList(List<A> first, List<B> second, BiFunction<A,B,R> combine, Function<A,R> onlyFirst, Function<B,R> onlySecond) {
			super(first,second,combine,onlyFirst,onlySecond);
		}
	}
	
	private static final BiFunction<Object,Object,Entry<Object,Object>> defaultZipper = (a,b) -> new SimpleImmutableEntry<>(a,b);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	static <A,B> BiFunction<A,B,Entry<A,B>> defaultZipper() { return (BiFunction)defaultZipper; }
}
