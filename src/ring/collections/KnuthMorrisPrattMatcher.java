package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.ConcurrentModificationException;
import java.util.Objects;

import ring.func.Equalizer;

//https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm
class KnuthMorrisPrattMatcher<E> {
	private final List<E> pattern; final Equalizer<? super E> eq;
	private final int[] mask;
	
	public KnuthMorrisPrattMatcher(List<E> pattern, Equalizer<? super E> eq) {
		this.pattern = pattern; this.eq = Objects.requireNonNull(eq);
		mask = new int[pattern.size()+1];
		for(int i = 0, j = -1; ; i++,j++) {
			mask[i] = j;
			if(i == mask.length-1) break;
			while(j >= 0 && !eq.equals(pattern.get(i),pattern.get(j))) j = mask[j];
		}
	}
	
	public int findFirstIn(List<E> text) {
		if(mask.length-1 != pattern.size()) throw new ConcurrentModificationException();
		if(mask.length-1 > text.size() || mask.length <= 1) return -1;
		var textElems = text.enumerate();
		for(int j = 0; textElems.next(); ) {
			while(j >= 0 && !eq.equals(textElems.curr(),pattern.get(j))) j = mask[j];
			j++;
			if(j == mask.length-1) return textElems.index()+1-j;
		}
		return -1;
	}
	
	public static <E> int findFirst(List<E> text, List<E> pattern, Equalizer<? super E> eq) {
		return text.size() < pattern.size() ? -1 : new KnuthMorrisPrattMatcher<>(pattern,eq).findFirstIn(text);
	}
	
	public static <E> int findLast(List<E> text, List<E> pattern, Equalizer<? super E> eq) {
		int ret = text.inverse().indexOfSegment(pattern.inverse(),eq);
		return ret < 0 ? ret : text.size()-pattern.size()-ret;
	}
}
