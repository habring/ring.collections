package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Comparator;
import java.util.Objects;

class TimSort<E> {
	private static final int RUN = 10;
	private final List<E> l;
	private final Comparator<? super E> c;
	private final List<?> tmp;
	
	private TimSort(List<E> l, Comparator<? super E> c) {
		this.l = Objects.requireNonNull(l); this.c = Objects.requireNonNull(c);
		this.tmp = List.of(new Object[l.size()],true);
	}
	
	@SuppressWarnings("unchecked")
	public static <E> void sort(List<E> l, Comparator<? super E> cmp) {
		new TimSort<>(l,cmp != null ? cmp : (Comparator<E>)Comparator.naturalOrder()).sort();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void sort() {
		for(int i = 0; i < l.size(); i+=RUN) insertionSort(i,i+RUN);
		List<?> src = l; List<?> dest = tmp;
		for(int run = RUN; run < l.size(); run*=2) {
			for(int i = 0; i < l.size(); i+=run*2) {
				mergeSort(src,i,Math.min(i+run,l.size()),Math.min(i+run*2,l.size()),dest);
			}
			var tmp = src; src = dest; dest = tmp;
		}
		if(dest == l) List.copy(tmp,0,(List)l,0,tmp.size());
		tmp.replaceAll(x->null); //simplifies garbage collection
	}
	
	@SuppressWarnings("TODO benchmark: second implementation should theoretically be faster")
	private void insertionSort(int from, int to) {
		to = Math.min(to,l.size());
		for(int i = from+1; i < to; i++) {
			var x = l.get(i);
			int j = i-1;
			for(; j >= from; j--) {
				var y = l.get(j);
				var cmp = c.compare(x,y);
				if(cmp >= 0) break;
				l.set(j+1,y);
			}
			l.set(j+1,x);
			/*
			var j = l.segment(from,i).binarySearch(x);
			if(j < 0) j = -j-1;
			j += from;
			if(j == i) continue;
			List.copy(l,j,l,j+1,i-j); l.set(j,x);
			*/
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void mergeSort(List src, int from, int mid, int to, List dest) {
		int i = from, j = mid;
		for(int k = from; k < to; k++) {
			if(i < mid && (j >= to || c.compare((E)src.get(i),(E)src.get(j)) <= 0)) {
				dest.set(k,src.get(i)); i++;
			}
			else { dest.set(k,src.get(j)); j++; }
		}
	}
}
