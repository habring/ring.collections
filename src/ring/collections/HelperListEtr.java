package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import static ring.collections.HelperEtr.defaultFindFirst;

import java.util.NoSuchElementException;
import java.util.Objects;

import ring.collections.HelperEtr.ProxyEtr1;
import ring.func.Equalizer;
import ring.util.ArrayUtil;

class HelperListEtr {
	private static abstract class AbstractIdxEtr<E> extends AbstractIndexedEnumerator<E> {
		final int size;
		
		public AbstractIdxEtr(int size) { this.size = ArrayUtil.checkSize(size); }
		
		@Override public int size() { return size; }
	}
	
	static class RepeatedEtrSingle<E> extends AbstractIdxEtr<E> {
		final E elem;
		
		RepeatedEtrSingle(E elem, int times) { super(times); this.elem = elem; }
		
		@Override public E curr() { ArrayUtil.checkIndex(size,index()); return elem; }
		
		@Override
		public boolean findFirst(E elem) {
			if(Objects.equals(this.elem,elem)) return next();
			jumpToEnd(); return false;
		}
	}
	
	static class ArrayEtr<E> extends AbstractIdxEtr<E> {
		final Object[] arr; final int from;
		
		ArrayEtr(Object[] arr, int from, int to) {
			super(to-from); this.arr = Objects.requireNonNull(arr); checkIndex(arr,from,to); this.from = from;
		}
		
		static void checkIndex(Object[] arr, int from, int to) {
			ArrayUtil.checkIndexIncl(arr.length,from); ArrayUtil.checkIndexIncl(arr.length,to);
		}
		
		int index0() { return from + ArrayUtil.checkIndex(size,index()); }
		
		@SuppressWarnings("unchecked")
		@Override public E curr() { return (E)arr[index0()]; }
	}
	
	static class ArrayEtrWritable<E> extends ArrayEtr<E> {
		ArrayEtrWritable(Object[] arr, int from, int to) { super(arr,from,to); }
		
		@SuppressWarnings("unchecked")
		@Override public E set(E elem) { int i = index0(); E ret = (E)arr[i]; arr[i] = elem; return ret; }
	}
	
	static class CommonPrefixEtr<E> extends ProxyEtr1<E> {
		final Enumerator<E> elems2; final Equalizer<? super E> eq;
		boolean end;
		
		CommonPrefixEtr(Enumerator<E> elems1, Enumerator<E> elems2, Equalizer<? super E> eq) {
			super(elems1); this.elems2 = Objects.requireNonNull(elems2); this.eq = Objects.requireNonNull(eq);
		}
		
		@Override
		public E curr() {
			if(!end) return elems.curr();
			else throw new NoSuchElementException();
		}
		
		@Override public int index() { return elems.index(); }
		
		@Override public int size() { throw new UnsupportedOperationException(); }
		
		@Override public void jumpTo(int i) { throw new UnsupportedOperationException(); }
		
		@Override public boolean findFirst(E elem) { return defaultFindFirst(this,elem); }
		
		@Override
		public boolean next() {
			if(end) return false;
			var tmp = elems.next() && elems2.next();
			if(tmp && !eq.equals(elems.curr(),elems2.curr())) { end = true; return false; }
			else return tmp;
		}
		
		@Override
		public boolean prev() {
			var ret = elems.prev() && elems2.prev();
			if(ret) end = false;
			return ret;
		}
	}
}
