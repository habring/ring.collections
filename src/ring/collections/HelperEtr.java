package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import ring.util.ArrayUtil;
import ring.util.IndexException;

class HelperEtr {
	static abstract class ProxyEtr2<T,I> implements Enumerator<T> {
		protected final Enumerator<I> elems;
		
		ProxyEtr2(Enumerator<I> elems) { this.elems = Objects.requireNonNull(elems); }
		
		@Override public int index() { return elems.index(); }
		
		@Override public int size() { return elems.size(); }
		
		@Override public void jumpToStart() { elems.jumpToStart(); }
		
		@Override public void jumpToEnd() { elems.jumpToEnd(); }
		
		@Override public void jumpTo(int i) { elems.jumpTo(i); }
		
		@Override public boolean next() { return elems.next(); }
		
		@Override public boolean prev() { return elems.prev(); }
	}
	
	static abstract class ProxyEtr1<E> extends ProxyEtr2<E,E> {
		ProxyEtr1(Enumerator<E> elems) { super(elems); }
		
		@Override public E curr() { return elems.curr(); }
		
		@Override public boolean findFirst(E elem) { return elems.findFirst(elem); }
	}
	
	static class ImmEtr<E> extends ProxyEtr1<E> {
		ImmEtr(Enumerator<E> elems) { super(elems); }
		
		@Override public Enumerator<E> immutable() { return this; }
	}
	
	static class InvEtr<E> extends ProxyEtr1<E> {
		InvEtr(Enumerator<E> elems) { super(elems); }
		
		private int i(int i) { return elems.size()-1-i; }
		
		@Override public int index() { return i(elems.index()); }
		
		@Override public void jumpToStart() { elems.jumpToEnd(); }
		
		@Override public void jumpToEnd() { elems.jumpToStart(); }
		
		@Override public void jumpTo(int i) { ArrayUtil.checkIndexEtr(size(),i); elems.jumpTo(i(i)); }
		
		@Override public boolean findFirst(E elem) { return defaultFindFirst(this,elem); }
		
		@Override public boolean next() { return elems.prev(); }
		
		@Override public boolean prev() { return elems.next(); }
		
		@Override public E set(E elem) { return elems.set(elem); }
		
		@Override
		public void add(E elem) {
			if(!elems.prev()) throw new IndexException(-1,size(),size(),true,false);
			try { elems.add(elem);
			} catch(RuntimeException exc) { elems.next(); throw exc; }
		}
		
		@Override
		public int addAll(Collection<? extends E> c) {
			if(!elems.prev()) throw new IndexException(-1,size(),size(),true,false);
			int ret;
			try { ret = elems.addAll(c.inverse());
			} catch(RuntimeException exc) { elems.next(); throw exc; }
			for(int i = 1; i < ret; i++) elems.prev();
			return ret;
		}
		
		@Override public E remove() { return elems.remove(); }
		
		@Override public void flush() { elems.flush(); }
		
		@Override public void close() { elems.close(); }
		
		@Override public Enumerator<E> inverse() { return elems; }
	}
	
	static class MappedEtr<T,I> extends ProxyEtr2<T,I> {
		final Function<? super I,T> apply;
		
		MappedEtr(Enumerator<I> elems, Function<? super I,T> apply) { super(elems); this.apply = Objects.requireNonNull(apply); }
		
		@Override public T curr() { return apply.apply(elems.curr()); }
		
		@Override public T remove() { return apply.apply(elems.remove()); }
		
		@Override public void flush() { elems.flush(); }
		
		@Override public void close() { elems.close(); }
	}
	
	static class BiMappedEtr<T,I> extends MappedEtr<T,I> {
		final Function<T,? extends I> revert;
		
		BiMappedEtr(Enumerator<I> elems, Function<? super I,T> apply, Function<T,? extends I> revert) {
			super(elems,apply); this.revert = Objects.requireNonNull(revert);
		}
		
		@Override public boolean findFirst(T elem) { return elems.findFirst(revert.apply(elem)); }
		
		@Override public T set(T elem) { return apply.apply(elems.set(revert.apply(elem))); }
		
		@Override public void add(T elem) { elems.add(revert.apply(elem)); }
		
		@Override public int addAll(Collection<? extends T> c) { return elems.addAll(c.map(revert)); }
	}
	
	static class FilteredEtr<E> extends ProxyEtr1<E> {
		final Predicate<? super E> filter;
		
		FilteredEtr(Enumerator<E> elems, Predicate<? super E> filter) { super(elems); this.filter = Objects.requireNonNull(filter); }
		
		@Override public int index() { throw new UnsupportedOperationException(); }
		
		@Override public int size() { throw new UnsupportedOperationException(); }
		
		@Override public void jumpTo(int i) { throw new UnsupportedOperationException(); }
		
		@Override
		public boolean findFirst(E elem) {
			if(filter.test(elem)) return elems.findFirst(elem);
			else { jumpToEnd(); return false; }
		}
		
		@Override public boolean next() { return elems.findFirstMatch(filter); }
		
		@Override
		public boolean prev() {
			while(elems.prev()) {
				if(filter.test(elems.curr())) return true;
			}
			return false;
		}
	}
	
	static class TypeFilteredEtr<T,I> extends ProxyEtr2<T,I> {
		final Class<T> filter;
		
		TypeFilteredEtr(Enumerator<I> elems, Class<T> filter) { super(elems); this.filter = Objects.requireNonNull(filter); }
		
		@Override public T curr() { return filter.cast(elems.curr()); }
		
		@Override public int index() { throw new UnsupportedOperationException(); }
		
		@Override public int size() { throw new UnsupportedOperationException(); }
		
		@Override public void jumpTo(int i) { throw new UnsupportedOperationException(); }
		
		@Override
		public boolean next() {
			while(elems.next()) {
				if(filter.isInstance(elems.curr())) return true;
			}
			return false;
		}
		
		@Override
		public boolean prev() {
			while(elems.prev()) {
				if(filter.isInstance(elems.curr())) return true;
			}
			return false;
		}
	}
	
	static class AsLongAsEtr<E> extends FilteredEtr<E> {
		boolean end;
		
		AsLongAsEtr(Enumerator<E> elems, Predicate<? super E> p) { super(elems,p); }
		
		@Override
		public E curr() {
			if(!end) return elems.curr();
			else throw new NoSuchElementException();
		}
		
		@Override public int index() { return elems.index(); }
		
		@Override public boolean findFirst(E elem) { return defaultFindFirst(this,elem); }
		
		@Override
		public boolean next() {
			if(end) return false;
			var tmp = elems.next();
			if(tmp && !filter.test(elems.curr())) { end = true; return false; }
			else return tmp;
		}
		
		@Override
		public boolean prev() {
			var ret = elems.prev();
			if(ret) end = false;
			return ret;
		}
	}
	
	static class EmptyEtr<E> implements Enumerator<E> {
		@SuppressWarnings("rawtypes")
		private static final EmptyEtr instance = new EmptyEtr<>();
		
		@SuppressWarnings("unchecked")
		static <E> EmptyEtr<E> get() { return instance; }
		
		private EmptyEtr() {}
		
		@Override public E curr() { throw new NoSuchElementException(); }
		
		@Override public int index() { return -1; }
		
		@Override public int size() { return 0; }
		
		@Override public void jumpTo(int i) { if(i != -1 && i != 0) throw new IndexException(0,i); }
		
		@Override public boolean next() { return false; }
		
		@Override public boolean prev() { return false; }
		
		@Override public Enumerator<E> immutable() { return this; }
	}
	
	static class ConcatEtr<E> implements Enumerator<E> {
		final Enumerator<? extends Enumerable<E>> elemsAll;
		Enumerator<E> elems = Enumerator.empty();
		
		ConcatEtr(Enumerator<? extends Enumerable<E>> elems) { this.elemsAll = Objects.requireNonNull(elems); }
		
		@Override public E curr() { return elems.curr(); }
		
		@Override public void jumpToStart() { elemsAll.jumpToStart(); elems = Enumerator.empty(); }
		
		@Override public void jumpToEnd() { elemsAll.jumpToEnd(); elems = Enumerator.empty(); }
		
		void setElems() {
			elems.close();
			elems = elemsAll.curr().enumerate();
		}
		
		@Override
		public boolean findFirst(E elem) {
			while(true) {
				if(elems.findFirst(elem)) return true;
				if(!elemsAll.next()) return false;
				setElems();
			}
		}
		
		@Override
		public boolean next() {
			while(true) {
				if(elems.next()) return true;
				if(!elemsAll.next()) return false;
				setElems();
			}
		}
		
		@Override
		public boolean prev() {
			while(true) {
				if(elems.prev()) return true;
				if(!elemsAll.prev()) return false;
				setElems(); elems.jumpToEnd();
			}
		}
		
		@Override public E set(E elem) { return elems.set(elem); }
		
		@Override public void add(E elem) { elems.add(elem); }
		
		@Override public int addAll(Collection<? extends E> c) { return elems.addAll(c); }
		
		@Override public E remove() { return elems.remove(); }
		
		@Override public void flush() { elems.flush(); }
		
		@Override public void close() { elems.close(); }
	}
	
	static class ZippedBase<R,E1,E2,C1,C2> {
		static final Function<Object,Object> defaultOnlyFunc = x -> { throw new NoSuchElementException(); };
		final C1 first; final C2 second;
		final BiFunction<E1,E2,R> combine; final Function<E1,R> onlyFirst; final Function<E2,R> onlySecond;
		
		@SuppressWarnings({ "rawtypes", "unchecked" })
		ZippedBase(C1 first, C2 second, BiFunction<E1,E2,R> combine, Function<E1,R> onlyFirst, Function<E2,R> onlySecond) {
			this.first = Objects.requireNonNull(first); this.second = Objects.requireNonNull(second);
			this.combine = Objects.requireNonNull(combine); this.onlyFirst = onlyFirst != null ? onlyFirst : (Function)defaultOnlyFunc;
			this.onlySecond = onlySecond != null ? onlySecond : (Function)defaultOnlyFunc;
		}
	}
	
	/**Regulus Arcturus Black ;)*/
	static class ZippedEtr<R,A,B> extends ZippedBase<R,A,B,Enumerator<A>,Enumerator<B>> implements Enumerator<R> {
		boolean has1, has2;
		
		ZippedEtr(Enumerator<A> first, Enumerator<B> second, BiFunction<A,B,R> combine, Function<A,R> onlyFirst, Function<B,R> onlySecond) {
			super(first,second,combine,onlyFirst,onlySecond);
		}
		
		@Override
		public R curr() {
			if(has1 && has2) return combine.apply(first.curr(),second.curr());
			else if(has1) return onlyFirst.apply(first.curr());
			else if(has2) return onlySecond.apply(second.curr());
			else throw new NoSuchElementException();
		}
		
		@Override
		public int index() {
			if(has1 && has2) {
				int i = first.index();
				if(i == second.index()) return i;
				throw new IllegalStateException();
			}
			else if(has1) return first.index();
			else if(has2) return second.index();
			else return first.index() >= 0 || second.index() >= 0 ? size() : -1;
		}
		
		@Override
		public int size() {
			var has1 = onlyFirst != defaultOnlyFunc; var has2 = onlySecond != defaultOnlyFunc;
			int s1 = first.size(), s2 = second.size();
			if(has1 && has2) return Math.max(s1,s2);
			else if(has1) return s1;
			else if(has2) return s2;
			else return Math.min(s1,s2);
		}
		
		@Override public void jumpToStart() { first.jumpToStart(); second.jumpToStart(); has1 = false; has2 = false; }
		
		@Override public void jumpToEnd() { first.jumpToEnd(); second.jumpToEnd(); has1 = false; has2 = false; }
		
		@Override
		public boolean next() {
			has1 = first.next(); has2 = second.next();
			if(has1 && has2) return true;
			else if(has1) return has1 = onlyFirst != defaultOnlyFunc;
			else if(has2) return has2 = onlySecond != defaultOnlyFunc;
			else return false;
		}
		
		@Override
		public R remove() {
			if(has1 && has2) return combine.apply(first.remove(),second.remove());
			else if(has1) return onlyFirst.apply(first.remove());
			else if(has2) return onlySecond.apply(second.remove());
			else throw new NoSuchElementException();
		}
		
		@Override public void flush() { first.flush(); second.flush(); }
		
		@Override public void close() { first.close(); second.close(); }
	}
	
	static class RepeatedEtr<E> implements Enumerator<E> {
		final Enumerator<E> elems;
		int currIdx; final int size;
		
		RepeatedEtr(Enumerator<E> elems, int times) {
			this.elems = Objects.requireNonNull(elems); this.size = times;
		}
		
		@Override public E curr() { return elems.curr(); }
		
		@Override public int index() { return currIdx*elems.size() + elems.index(); }
		
		@Override public int size() { return size*elems.size(); }
		
		@Override public void jumpToStart() { elems.jumpToStart(); currIdx = 0; }
		
		@Override public void jumpToEnd() { elems.jumpToEnd(); currIdx = size-1; }
		
		@Override
		public void jumpTo(int i) {
			var s = elems.size();
			ArrayUtil.checkIndexEtr(s*size,i);
			if(i < 0 || s <= 0) jumpToStart();
			else {
				int x = i % s, y = i / s;
				if(y >= size-1) jumpToEnd();
				else { elems.jumpTo(x); currIdx = y; }
			}
		}
		
		@Override
		public boolean findFirst(E elem) {
			if(elems.findFirst(elem)) return true;
			if(currIdx == size-1) return false;
			elems.jumpToStart(); currIdx++;
			if(elems.findFirst(elem)) return true;
			jumpToEnd(); return false;
		}
		
		@Override
		public boolean next() {
			if(elems.next()) return true;
			if(currIdx == size-1) return false;
			elems.jumpToStart(); currIdx++;
			if(elems.next()) return true;
			jumpToEnd(); return false;
		}
		
		@Override
		public boolean prev() {
			if(elems.prev()) return true;
			if(currIdx == 0) return false;
			elems.jumpToEnd(); currIdx--;
			if(elems.prev()) return true;
			jumpToStart(); return false;
		}
	}
	
	static <E> boolean defaultFindFirst(Enumerator<E> elems, E elem) {
		if(elem == null) {
			while(elems.next()) {
				if(elems.curr() == null) return true;
			}
		}
		else {
			while(elems.next()) {
				if(elem.equals(elems.curr())) return true;
			}
		}
		return false;
	}
}
