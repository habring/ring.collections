package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import ring.collections.HelperColl.EmptyList;
import ring.collections.HelperColl.ImmList;
import ring.collections.HelperColl.InvList;
import ring.collections.HelperColl.ZippedList;
import ring.collections.HelperEtr.RepeatedEtr;
import ring.collections.HelperListEtr.ArrayEtr;
import ring.collections.HelperListEtr.ArrayEtrWritable;
import ring.collections.HelperListEtr.CommonPrefixEtr;
import ring.collections.HelperListEtr.RepeatedEtrSingle;
import ring.collections.impl.ArrayList;
import ring.func.Equalizer;
import ring.func.IntBiConsumer;
import ring.util.ArrayUtil;
import ring.util.BinarySearchUtil.ListBinarySearchUtil;

public interface List<E> extends Collection<E> {
	/**Enumerates through the elements of this list starting at the given index (valid range [-1,{@link #size()}]).*/
	default Enumerator<E> enumerate(int startIdx) { var ret = enumerate(); ret.jumpTo(startIdx); return ret; }
	
	default int indexOf(E elem) {
		var elems = enumerate(); return elems.findFirst(elem) ? elems.index() : -1;
	}
	
	default int indexOfAny(Predicate<? super E> p) {
		var elems = enumerate(); return elems.findFirstMatch(p) ? elems.index() : -1;
	}
	
	default int lastIndexOf(E elem) {
		var elems = inverse().enumerate(); return elems.findFirst(elem) ? elems.index() : -1;
	}
	
	default int lastIndexOfAny(Predicate<? super E> p) {
		var elems = inverse().enumerate(); return elems.findFirstMatch(p) ? elems.index() : -1;
	}
	
	default int binarySearch(E elem, Comparator<? super E> cmp) {
		var ret = new ListBinarySearchUtil<>(this,cmp).binarySearch(elem); return ret != null ? ret : -1;
	}
	
	default E get(int i) { return enumerate(i).curr(); }
	
	default E set(int i, E elem) { try(var elems = enumerate(i)) { return elems.set(elem); } }
	
	@Override
	default void add(E elem) {
		try(var elems = enumerate()) { elems.jumpTo(elems.size()-1); elems.add(elem); }
	}
	
	default void add(int i, E elem) { try(var elems = enumerate(i-1)) { elems.add(elem); } }
	
	@Override
	default int addAll(Collection<? extends E> c) {
		try(var elems = enumerate()) { elems.jumpTo(elems.size()-1); return elems.addAll(c); }
	}
	
	default int addAll(int i, Collection<? extends E> c) { try(var elems = enumerate(i-1)) { return elems.addAll(c); } }
	
	default E removeAt(int i) { try(var elems = enumerate(i)) { return elems.remove(); } }
	
	default void removeRange(int from, int to) {
		try(var elems = enumerate(from)) {
			ArrayUtil.checkSubRange(elems.size(),from,to);
			for(int i = from; i < to; i++) {
				if(!elems.next()) throw new ConcurrentModificationException();
				elems.remove();
			}
		}
	}
	
	default void retainRange(int from, int to) {
		try(var elems = enumerate(to-1)) {
			ArrayUtil.checkSubRange(elems.size(),from,to);
			while(elems.next()) elems.remove();
			elems.jumpToStart();
			for(int i = 0; i < from; i++) {
				if(!elems.next()) throw new ConcurrentModificationException();
				elems.remove();
			}
		}
	}
	
//	constructors
	
	@Override default List<E> immutable() { return new ImmList<>(this); }
	
	@Override default List<E> inverse() { return new InvList<>(this); }
	
	default void revert() {
		try(var elems1 = enumerate(); var elems2 = enumerate()) {
			elems2.jumpToEnd();
			while(elems1.index() < elems2.index() && elems1.next() && elems2.prev()) {
				E e1 = elems1.set(elems2.curr()); elems1.flush(); elems2.set(e1); elems2.flush();
			}
		}
	}
	
	static <E> List<E> empty() { return EmptyList.get(); }
	
	static <E> List<E> of(E elem) { return repeat(elem,1); }
	
	@SafeVarargs
	static <E> List<E> of(E...elems) { return of(elems,false); }
	
	static <E> List<E> of(E[] elems, boolean writable) { return of(elems,0,elems.length,writable); }
	
	static <E> List<E> of(E[] elems, int from, int to) { return of(elems,from,to,false); }
	
	static <E> List<E> of(E[] elems, int from, int to, boolean writable) {
		ArrayEtr.checkIndex(elems,from,to);
		if(writable) return new AbstractList<>() {
			@Override public Enumerator<E> enumerate() { return new ArrayEtrWritable<>(elems,from,to); }
		};
		else return new AbstractList<>() {
			@Override public Enumerator<E> enumerate() { return new ArrayEtr<>(elems,from,to); }
		};
	}
	
//	functional
	
	default void forAllIndexed(IntBiConsumer<? super E> c) { enumerate().forRestIndexed(c); }
	
	default void setAll(IntFunction<? extends E> f) { enumerate().setRest(f); }
	
//	mapping
	
	@Override
	default <R> List<R> map(Function<? super E,R> f) {
		Objects.requireNonNull(f); var out = this;
		return new AbstractList<>() {
			@Override public Enumerator<R> enumerate() { return out.enumerate().map(f); }
		};
	}
	
	@Override
	default <R> List<R> map(Function<? super E,R> apply, Function<R,? extends E> revert) {
		Objects.requireNonNull(apply); Objects.requireNonNull(revert); var out = this;
		return new AbstractList<>() {
			@Override public Enumerator<R> enumerate() { return out.enumerate().map(apply,revert); }
		};
	}
	
//	zip and concat
	
	static <A,B> List<Entry<A,B>> zip(List<A> first, List<B> second) { return zip(first,second,HelperColl.defaultZipper()); }
	
	static <R,A,B> List<R> zip(List<A> first, List<B> second, BiFunction<A,B,R> combine) { return zip(first,second,combine,null,null); }
	
	static <R,A,B> List<R> zip(List<A> first, List<B> second, BiFunction<A,B,R> combine, Function<A,R> onlyFirst, Function<B,R> onlySecond) {
		return new ZippedList<>(first,second,combine,onlyFirst,onlySecond);
	}
	
	@Override
	default List<E> repeat(int times) {
		var out = this;
		return new AbstractList<>() {
			@Override public Enumerator<E> enumerate() { return new RepeatedEtr<>(out.enumerate(),times); }
		};
	}
	
	static <E> List<E> repeat(E elem, int times) {
		return new AbstractList<>() {
			@Override public Enumerator<E> enumerate() { return new RepeatedEtrSingle<>(elem,times); }
		};
	}
	
//	segment match, trim and split
	
	default boolean startsWith(List<E> l) { return startsWith(l,Equalizer.natural()); }
	
	default boolean startsWith(List<E> l, Equalizer<? super E> eq) {
		var elems = enumerate(); Objects.requireNonNull(eq);
		for(var e : l) {
			if(!elems.next()) return false;
			if(!eq.equals(elems.curr(),e)) return false;
		}
		return true;
	}
	
	default boolean endsWith(List<E> l) { return endsWith(l,Equalizer.natural()); }
	
	default boolean endsWith(List<E> l, Equalizer<? super E> eq) {
		var elems1 = enumerate(); var elems2 = l.enumerate(); Objects.requireNonNull(eq);
		elems1.jumpToEnd(); elems2.jumpToEnd();
		while(elems2.prev()) {
			if(!elems1.prev()) return false;
			if(!eq.equals(elems1.curr(),elems2.curr())) return false;
		}
		return true;
	}
	
	default Enumerable<E> commonPrefix(List<E> l) { return commonPrefix(l,Equalizer.natural()); }
	
	default Enumerable<E> commonPrefix(List<E> l, Equalizer<? super E> eq) {
		Objects.requireNonNull(l); Objects.requireNonNull(eq); return () -> new CommonPrefixEtr<>(enumerate(),l.enumerate(),eq);
	}
	
	default int indexOfSegment(List<E> segment) { return indexOfSegment(segment,Equalizer.natural()); }
	
	default int indexOfSegment(List<E> segment, Equalizer<? super E> eq) { return KnuthMorrisPrattMatcher.findFirst(this,segment,eq); }
	
	default int lastIndexOfSegment(List<E> segment) { return lastIndexOfSegment(segment,Equalizer.natural()); }
	
	default int lastIndexOfSegment(List<E> segment, Equalizer<? super E> eq) { return KnuthMorrisPrattMatcher.findLast(this,segment,eq); }
	
	default int trimStart(Predicate<? super E> p) {
		try(var elems = enumerate()) {
			int num = 0;
			while(elems.next()) {
				if(!p.test(elems.curr())) return num;
				elems.remove(); num++;
			}
			return num;
		}
	}
	
	default int trimEnd(Predicate<? super E> p) {
		try(var elems = enumerate()) {
			int num = 0; elems.jumpToEnd();
			while(elems.prev()) {
				if(!p.test(elems.curr())) return num;
				elems.remove(); num++;
			}
			return num;
		}
	}
	
	default int trim(Predicate<? super E> p) { return trimStart(p) + trimEnd(p); }
	
	default List<List<E>> splitCopy(List<E> pattern) { return splitCopy(pattern,Equalizer.natural()); }
	
	default List<List<E>> splitCopy(List<E> pattern, Equalizer<? super E> eq) { var l = split(pattern,eq); l.replaceAll(List::toList); return l; }
	
	default List<List<E>> split(List<E> pattern) { return split(pattern,Equalizer.natural()); }
	
	default List<List<E>> split(List<E> pattern, Equalizer<? super E> eq) {
		var ret = new ArrayList<List<E>>(); int from = 0;
		while(true) {
			int i = segmentFrom(from).indexOfSegment(pattern);
			if(i < 0) break;
			ret.add(segment(from,i)); from = i+pattern.size();
		}
		return ret;
	}
	
//	segment, copy and sort
	
	default List<E> segmentFrom(int from) { return segment(from,size()); }
	
	default List<E> segmentTo(int to) { return segment(0,to); }
	
	default List<E> segment(int from, int to) { return new Segment<>(this,from,to); }
	
	/**@see System#arraycopy(Object, int, Object, int, int)*/
	static <E> void copy(List<E> src, int srcPos, List<E> dest, int destPos, int length) {
		ArrayUtil.checkSubRange(src.size(),srcPos,srcPos+length);
		ArrayUtil.checkSubRange(dest.size(),destPos,destPos+length);
		if(srcPos >= destPos) {
			var s = src.enumerate(srcPos-1);
			try(var d = dest.enumerate(destPos-1)) {
				for(int i = 0; i < length; i++) {
					if(!s.next() || !d.next()) throw new ConcurrentModificationException();
					d.set(s.curr());
				}
			}
		}
		else {
			var s = src.enumerate(srcPos+length);
			try(var d = dest.enumerate(destPos+length)) {
				for(int i = 0; i < length; i++) {
					if(!s.prev() || !d.prev()) throw new ConcurrentModificationException();
					d.set(s.curr());
				}
			}
		}
	}
	
	default void sort(Comparator<? super E> cmp) { TimSort.sort(this,cmp); }
	
	@SuppressWarnings("unchecked")
	default boolean isSorted(Comparator<? super E> cmp) {
		if(cmp == null) cmp = (Comparator<E>)Comparator.naturalOrder();
		var elems = enumerate();
		if(!elems.next()) return true;
		E prev = elems.curr();
		while(elems.next()) {
			E curr = elems.curr();
			if(cmp.compare(prev,curr) > 0) return false;
			prev = curr;
		}
		return true;
	}
}
