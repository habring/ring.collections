package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

import ring.collections.HelperColl.ImmColl;
import ring.collections.HelperEtr.EmptyEtr;
import ring.collections.HelperEtr.ImmEtr;
import ring.collections.HelperEtr.ProxyEtr2;
import ring.collections.impl.ArrayList;
import ring.func.Equalizer;
import ring.util.ArrayUtil;

class HelperMap {
	static class ImmMap<K,V> extends ImmColl<Entry<K,V>> implements Map<K,V> {
		ImmMap(Map<K,V> map) { super(map); }
		
		@SuppressWarnings("unchecked")
		@Override public Equalizer<K> keyEqualizer() { return ((Map<K,V>)elems).keyEqualizer(); }
		
		@Override public Enumerator<Entry<K,V>> enumerate() { return new ImmMapEtr<>(elems.enumerate()); }
		
		@Override public Map<K,V> immutable() { return this; }
	}
	
	static class ImmMapEtr<K,V> extends ImmEtr<Entry<K,V>> {
		ImmMapEtr(Enumerator<Entry<K,V>> elems) { super(elems); }

		@Override public Entry<K,V> curr() { return new ImmEntry<>(elems.curr()); }
	}
	
	static class ProxyEntry<K,V> implements Entry<K,V> {
		final Entry<K,V> e;
		
		ProxyEntry(Entry<K,V> e) { this.e = Objects.requireNonNull(e); }
		
		@Override public K getKey() { return e.getKey(); }
		
		@Override public V getValue() { return e.getValue(); }
		
		@Override public V setValue(V value) { return e.setValue(value); }
		
		@Override public int hashCode() { return e.hashCode(); }
		
		@Override public boolean equals(Object obj) { return e.equals(obj); }
		
		@Override public String toString() { return e.toString(); }
	}
	
	static class ImmEntry<K,V> extends ProxyEntry<K,V> {
		ImmEntry(Entry<K, V> e) { super(e); }
		
		@Override public V setValue(V value) { throw new UnsupportedOperationException(); }
	}
	
	static class KeyCollection<K,V> extends AbstractCollection<K> {
		final Map<K,V> map;
		
		public KeyCollection(Map<K,V> map) { this.map = Objects.requireNonNull(map); }
		
		@Override public Enumerator<K> enumerate() { return new KeyEtr<>(map.enumerate()); }
		
		private static class KeyEtr<K,V> extends ProxyEtr2<K,Entry<K,V>> {
			KeyEtr(Enumerator<Entry<K,V>> elems) { super(elems); }
			
			@Override public K curr() { return elems.curr().getKey(); }
			
			@Override public boolean findFirst(K elem) { return elems.findFirst(new KeyEntry<>(elem)); }
			
			@Override public K remove() { return elems.remove().getKey(); }
			
			@Override public void flush() { elems.flush(); }
			
			@Override public void close() { elems.close(); }
		}
	}
	
	static class ValueCollection<K,V> extends AbstractCollection<V> {
		final Map<K,V> map;
		
		public ValueCollection(Map<K,V> map) { this.map = Objects.requireNonNull(map); }
		
		@Override public Enumerator<V> enumerate() { return new ValueEtr<>(map.enumerate()); }
		
		private static class ValueEtr<K,V> extends ProxyEtr2<V,Entry<K,V>> {
			ValueEtr(Enumerator<Entry<K,V>> elems) { super(elems); }
			
			@Override public V curr() { return elems.curr().getValue(); }
			
			@Override public V set(V elem) { return elems.curr().setValue(Objects.requireNonNull(elem)); }
			
			@Override public V remove() { return elems.remove().getValue(); }
			
			@Override public void flush() { elems.flush(); }
			
			@Override public void close() { elems.close(); }
		}
	}
	
	static class EmptyMap<K,V> extends AbstractMap<K,V> {
		@SuppressWarnings("rawtypes")
		private static final EmptyMap instance = new EmptyMap<>();
		
		@SuppressWarnings("unchecked")
		static <K,V> EmptyMap<K,V> get() { return instance; }
		
		private EmptyMap() {}

		@Override public Equalizer<K> keyEqualizer() { return Equalizer.natural(); }
		
		@Override public Enumerator<Entry<K,V>> enumerate() { return EmptyEtr.get(); }
		
		@Override public Map<K,V> immutable() { return this; }
	}
	
	static class SingletonMap<K,V> extends AbstractMap<K,V> {
		final Entry<K,V> elem; final Equalizer<K> eq;
		
		SingletonMap(K key, V value, Equalizer<K> eq) {
			this.elem = new SimpleImmutableEntry<>(key,value); this.eq = Objects.requireNonNull(eq);
		}
		
		@Override public Equalizer<K> keyEqualizer() { return eq; }
		
		@Override public Enumerator<Entry<K,V>> enumerate() { return new SingletonMapEtr<>(this); }
	}
	
	private static class SingletonMapEtr<K,V> extends AbstractIndexedEnumerator<Entry<K,V>> {
		final SingletonMap<K,V> owner;
		
		SingletonMapEtr(SingletonMap<K,V> owner) { this.owner = Objects.requireNonNull(owner); }
		
		@Override public Entry<K,V> curr() { ArrayUtil.checkIndex(size(),index()); return owner.elem; }
		
		@Override public int size() { return 1; }
		
		@Override
		public boolean findFirst(Entry<K,V> elem) {
			if(index() < 0 && owner.eq.equals(owner.elem.getKey(),elem.getKey()) &&
					(elem.getValue() == null || Objects.equals(owner.elem.getValue(),elem.getValue()))) {
				jumpTo(0); return true;
			}
			else { jumpToEnd(); return false; }
		}
	}
	
	static class MappedMapEtr<K,V,R> extends ProxyEtr2<Entry<K,R>,Entry<K,V>> {
		final BiFunction<? super K,? super V,R> apply;
		final Equalizer<K> eq;
		
		MappedMapEtr(Map<K,V> map, BiFunction<? super K,? super V,R> apply) {
			super(map.enumerate()); this.apply = Objects.requireNonNull(apply); this.eq = map.keyEqualizer();
		}
		
		Entry<K,R> i2e(Entry<K,V> e) { return new MappedEntry<>(this,e); }
		
		@Override public Entry<K,R> curr() { return i2e(elems.curr()); }
		
		@Override public Entry<K,R> remove() { return i2e(elems.remove()); }
		
		@Override public void flush() { elems.flush(); }
		
		@Override public void close() { elems.close(); }
	}
	
	static class BiMappedMapEtr<K,V,R> extends MappedMapEtr<K,V,R> {
		final BiFunction<? super K,? super R,V> revert;
		
		BiMappedMapEtr(Map<K,V> map, BiFunction<? super K,? super V,R> apply, BiFunction<? super K,? super R,V> revert) {
			super(map,apply); this.revert = Objects.requireNonNull(revert);
		}
		
		@Override Entry<K,R> i2e(Entry<K,V> e) { return new BiMappedEntry<>(this,e); }
		
		Entry<K,V> e2i(Entry<K,R> e) { return new SimpleImmutableEntry<>(e.getKey(),revert.apply(e.getKey(),e.getValue())); }
		
		@Override public boolean findFirst(Entry<K,R> elem) { return elems.findFirst(e2i(elem)); }
		
		@Override public Entry<K,R> set(Entry<K,R> elem) { return i2e(elems.set(e2i(elem))); }
		
		@Override public void add(Entry<K,R> elem) { elems.add(e2i(elem)); }
		
		@Override public int addAll(Collection<? extends Entry<K,R>> c) { return elems.addAll(c.map(this::e2i)); }
	}
	
	static abstract class AbstractEntry<K,V> implements Entry<K,V> {
		abstract Equalizer<K> keyEqualizer();
		
		@Override public int hashCode() { return keyEqualizer().hashCode(getKey()) ^ getValue().hashCode(); }
		
		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public boolean equals(Object obj) {
			return obj instanceof Entry e && keyEqualizer().equals(getKey(),(K)e.getKey()) && Objects.equals(getValue(),e.getValue());
		}
		
		@Override public String toString() { return getKey() + "=" + getValue(); }
	}
	
	private static class MappedEntry<K,T,I,MapEtr extends MappedMapEtr<K,I,T>> extends AbstractEntry<K,T> {
		final MapEtr owner;
		final Entry<K,I> e;
		
		MappedEntry(MapEtr owner, Entry<K,I> e) { this.owner = Objects.requireNonNull(owner); this.e = Objects.requireNonNull(e); }

		@Override Equalizer<K> keyEqualizer() { return owner.eq; }
		
		@Override public K getKey() { return e.getKey(); }
		
		@Override public T getValue() { return owner.apply.apply(e.getKey(),e.getValue()); }
		
		@Override public T setValue(T value) { throw new UnsupportedOperationException(); }
	}
	
	private static class BiMappedEntry<K,T,I> extends MappedEntry<K,T,I,BiMappedMapEtr<K,I,T>> {
		BiMappedEntry(BiMappedMapEtr<K,I,T> owner, Entry<K,I> e) { super(owner,e); }
		
		@Override public T setValue(T value) { return owner.apply.apply(e.getKey(),owner.revert.apply(e.getKey(),value)); }
	}
	
	static class KVMappedMap<K,V,A,B> extends AbstractMap<A,B> {
		final Map<K,V> map;
		final Function<Entry<K,V>,Entry<A,B>> apply; final Function<Entry<A,B>,Entry<K,V>> revert;
		final Equalizer<A> eq;
		
		KVMappedMap(Map<K,V> map, Function<Entry<K,V>,Entry<A,B>> apply, Function<Entry<A,B>,Entry<K,V>> revert) {
			this.map = map; this.apply = Objects.requireNonNull(apply); this.revert = Objects.requireNonNull(revert);
			this.eq = map.keyEqualizer().map(k->revert.apply(new KeyEntry<A,B>(k)).getKey());
		}
		
		@Override public Equalizer<A> keyEqualizer() { return eq; }
		
		@Override public Enumerator<Entry<A,B>> enumerate() { return new KVMappedMapEtr<>(this); }
	}
	
	static class KVMappedMapEtr<K,V,A,B> extends ProxyEtr2<Entry<A,B>,Entry<K,V>> {
		final KVMappedMap<K,V,A,B> owner;
		
		KVMappedMapEtr(KVMappedMap<K,V,A,B> owner) { super(owner.map.enumerate()); this.owner = owner; }
		
		@Override public Entry<A,B> curr() { return new KVMappedEntry<>(this,elems.curr()); }
		
		@Override public boolean findFirst(Entry<A,B> elem) { return elems.findFirst(owner.revert.apply(elem)); }
		
		@Override public Entry<A,B> set(Entry<A,B> elem) { return owner.apply.apply(elems.set(owner.revert.apply(elem))); }
		
		@Override public void add(Entry<A,B> elem) { elems.add(owner.revert.apply(elem)); }
		
		@Override public int addAll(Collection<? extends Entry<A,B>> c) { return elems.addAll(c.map(owner.revert)); }
		
		@Override public Entry<A,B> remove() { return owner.apply.apply(elems.remove()); }
		
		@Override public void flush() { elems.flush(); }
		
		@Override public void close() { elems.close(); }
	}
	
	private static class KVMappedEntry<K,V,A,B> extends AbstractEntry<A,B> {
		final KVMappedMapEtr<K,V,A,B> owner;
		final Entry<K,V> e;
		
		KVMappedEntry(KVMappedMapEtr<K,V,A,B> owner, Entry<K,V> e) { this.owner = Objects.requireNonNull(owner); this.e = Objects.requireNonNull(e); }
		
		@Override Equalizer<A> keyEqualizer() { return owner.owner.eq; }
		
		@Override public A getKey() { return owner.owner.apply.apply(e).getKey(); }
		
		@Override public B getValue() { return owner.owner.apply.apply(e).getValue(); }
		
		@Override
		public B setValue(B value) {
			var e1 = owner.owner.apply.apply(e);
			var e2 = owner.owner.revert.apply(new SimpleImmutableEntry<>(e1.getKey(),value));
			if(!owner.owner.map.keyEqualizer().equals(e2.getKey(),e.getKey())) throw new IllegalStateException();
			e.setValue(e2.getValue()); return e1.getValue();
		}
	}
	
	static <E> void copy(Enumerable<E> src, Enumerator<E> dest, DuplicateAction duplicateAction, Function<E,E> beforeFind) {
		switch(duplicateAction) {
		case error:
			for(var e : src) { dest.add(e); }
			break;
		case overwrite:
			for(var e : src) {
				dest.jumpToStart();
				E e2 = beforeFind.apply(e);
				if(!dest.findFirst(e2)) dest.add(e);
			}
			break;
		case remove:
			var del = new ArrayList<E>();
			for(var e : src) {
				dest.jumpToStart();
				E e2 = beforeFind.apply(e);
				if(dest.findFirst(e2)) del.add(e2);
				else dest.add(e);
			}
			for(var e : del) {
				dest.jumpToStart();
				if(dest.findFirst(e)) dest.remove();
			}
			break;
		default: throw new NullPointerException();
		}
	}
}
