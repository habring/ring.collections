package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.function.*;

import ring.collections.HelperColl.ConcatEnumerable;
import ring.collections.HelperColl.ImmEnumerable;
import ring.collections.HelperColl.InvEnumerable;
import ring.collections.HelperColl.ZippedEnumerable;
import ring.collections.HelperEtr.RepeatedEtr;
import ring.collections.impl.ArrayList;
import ring.collections.impl.HashMap;
import ring.collections.impl.HashSet;
import ring.func.Equalizer;
import ring.jcollections.JWrapper;

@SuppressWarnings("TODO recurse(BF|DF)()")
public interface Enumerable<E> extends Iterable<E> {
	Enumerator<E> enumerate();
	
//	constructors
	
	default Enumerable<E> immutable() { return new ImmEnumerable<>(this); }
	
	default Enumerable<E> inverse() { return new InvEnumerable<>(this); }
	
//	single element interface
	
	default boolean hasNone() { return !enumerate().next(); }
	
	default boolean hasSingle() { var elems = enumerate(); return elems.next() && !elems.next(); }
	
	default boolean hasAny() { return enumerate().next(); }
	
	default boolean hasMultiple() { var elems = enumerate(); return elems.next() && elems.next(); }
	
	default E first() {
		var elems = enumerate();
		if(elems.next()) return elems.curr();
		throw new NoSuchElementException();
	}
	
	default E firstOr(E defaultValue) { var elems = enumerate(); return elems.next() ? elems.curr() : defaultValue; }
	
	default E firstMatch(Predicate<? super E> p) {
		var elems = enumerate();
		if(elems.findFirstMatch(p)) return elems.curr();
		throw new NoSuchElementException();
	}
	
	default E firstMatchOr(E defaultValue, Predicate<? super E> p) { var elems = enumerate(); return elems.findFirstMatch(p) ? elems.curr() : defaultValue; }
	
	default E single() {
		var elems = enumerate();
		if(!elems.next()) throw new NoSuchElementException();
		E ret = elems.curr();
		if(elems.next()) throw new NoSuchElementException();
		return ret;
	}
	
	default E singleOr(E lessValue) {
		var elems = enumerate();
		if(!elems.next()) return lessValue;
		E ret = elems.curr();
		if(elems.next()) throw new NoSuchElementException();
		return ret;
	}
	
	default E singleMatch(Predicate<? super E> p) {
		var elems = enumerate();
		if(!elems.findFirstMatch(p)) throw new NoSuchElementException();
		E ret = elems.curr();
		if(elems.next()) throw new NoSuchElementException();
		return ret;
	}
	
	default E singleMatchOr(E lessValue, Predicate<? super E> p) {
		var elems = enumerate();
		if(!elems.findFirstMatch(p)) return lessValue;
		E ret = elems.curr();
		if(elems.next()) throw new NoSuchElementException();
		return ret;
	}
	
//	functional
	
	default void forAll(Consumer<? super E> c) { enumerate().forRest(c); }
	
	default <R> R fold(R first, BiFunction<R,? super E,R> f) { return enumerate().foldRest(first,f); }
	
	default <R> R fold(R first, BiFunction<R,? super E,R> f, Predicate<? super R> interrupt) { return enumerate().foldRest(first,f,interrupt); }
	
	default boolean anyMatch(Predicate<? super E> p) { return enumerate().findFirstMatch(p); }
	
	default boolean allMatch(Predicate<? super E> p) { return enumerate().testRest(p); }
	
	default int count() {
		int ret = 0; var elems = enumerate();
		while(elems.next()) ret++;
		return ret;
	}
	
	default int count(Predicate<? super E> p) { return enumerate().countRest(p); }
	
	default void replaceAll(Function<? super E, ? extends E> f) { enumerate().replaceRest(f); }
	
//	mapping and filtering
	
	default <R> Enumerable<R> map(Function<? super E,R> f) { Objects.requireNonNull(f); return () -> enumerate().map(f); }
	
	default <R> Enumerable<R> map(Function<? super E,R> apply, Function<R,? extends E> revert) {
		Objects.requireNonNull(apply); Objects.requireNonNull(revert); return () -> enumerate().map(apply,revert);
	}
	
	default <R> Enumerable<R> mapMulti(Function<E,? extends Enumerable<R>> f) { return new ConcatEnumerable<>(map(f)); }
	
	default Enumerable<E> filter(Predicate<? super E> filter) { Objects.requireNonNull(filter); return () -> enumerate().filter(filter); }
	
	default <R> Enumerable<R> filterType(Class<R> type) { Objects.requireNonNull(type); return () -> enumerate().filterType(type); }
	
	default Enumerable<E> asLongAs(Predicate<E> p) { Objects.requireNonNull(p); return () -> enumerate().asLongAs(p); }
	
//	groupBy
	
	default <K> Map<K,E> groupByUnique(Function<? super E,K> getKey) { return groupByUnique(getKey,Equalizer.natural()); }
	
	default <K> Map<K,E> groupByUnique(Function<? super E,K> getKey, Equalizer<K> eq) { return groupByUnique(getKey,e->e,eq); }
	
	default <K,V> Map<K,V> groupByUnique(Function<? super E,K> getKey, Function<? super E,V> getValue) { return groupByUnique(getKey,getValue,Equalizer.natural()); }
	
	default <K,V> Map<K,V> groupByUnique(Function<? super E,K> getKey, Function<? super E,V> getValue, Equalizer<K> eq) {
		return groupBy(getKey, getValue, (r,e) -> {
			throw new UnsupportedOperationException("multiple values for key "+getKey.apply(e));
		}, eq);
	}
	
	default <K> Map<K,List<E>> groupBy(Function<? super E,K> getKey) { return groupBy(getKey,Equalizer.natural()); }
	
	default <K> Map<K,List<E>> groupBy(Function<? super E,K> getKey, Equalizer<K> eq) {
		return groupBy(getKey, e -> {
			var l = new ArrayList<E>(); l.add(e); return l;
		}, (l,e) -> { l.add(e); return l; }, eq);
	}
	
	default <K,V> Map<K,V> groupBy(Function<? super E,K> getKey, Function<? super E,V> firstValue, BiFunction<V,? super E,V> foldValue) {
		return groupBy(getKey,firstValue,foldValue,Equalizer.natural());
	}
	
	default <K,V> Map<K,V> groupBy(Function<? super E,K> getKey, Function<? super E,V> firstValue, BiFunction<V,? super E,V> foldValue, Equalizer<K> eq) {
		Objects.requireNonNull(getKey); Objects.requireNonNull(firstValue); Objects.requireNonNull(foldValue); Objects.requireNonNull(eq);
		var ret = new HashMap<K,V>(eq);
		try(var tmp = ret.enumerate()) {
			for(var e : this) {
				tmp.jumpToStart();
				K key = getKey.apply(e);
				if(key == null) continue;
				if(tmp.findFirst(new KeyEntry<>(key))) {
					var entry = tmp.curr();
					V value = foldValue.apply(entry.getValue(),e);
					entry.setValue(Objects.requireNonNull(value));
				}
				else {
					V value = firstValue.apply(e);
					tmp.add(new SimpleEntry<>(key,Objects.requireNonNull(value)));
				}
			}
		}
		return ret;
	}
	
//	zip and concat
	
	static <A,B> Enumerable<Entry<A,B>> zip(Enumerable<A> first, Enumerable<B> second) { return zip(first,second,HelperColl.defaultZipper()); }
	
	static <R,A,B> Enumerable<R> zip(Enumerable<A> first, Enumerable<B> second, BiFunction<A,B,R> combine) { return zip(first,second,combine,null,null); }
	
	static <R,A,B> Enumerable<R> zip(Enumerable<A> first, Enumerable<B> second, BiFunction<A,B,R> combine, Function<A,R> onlyFirst, Function<B,R> onlySecond) {
		return new ZippedEnumerable<>(first,second,combine,onlyFirst,onlySecond);
	}
	
	default Enumerable<E> repeat(int times) { return () -> new RepeatedEtr<>(enumerate(),times); }
	
	@SafeVarargs static <E> Enumerable<E> concat(Enumerable<E>...elems) { return new ConcatEnumerable<>(List.of(elems)); }
	
	static <E> Enumerable<E> concat(Enumerable<? extends Enumerable<E>> elems) { return new ConcatEnumerable<>(elems); }
	
//	hashCode and equals
	
	static <E> int hashCode(Enumerable<E> elems) { return hashCode(elems,Equalizer.natural()); }
	
	static <E> int commutativeHashCode(Enumerable<E> elems) { return commutativeHashCode(elems,Equalizer.natural()); }
	
	static <E> int hashCode(Enumerable<E> elems, Equalizer<? super E> eq) {
		int ret = 1; Objects.requireNonNull(eq);
		for(E e : elems) ret = 31*ret + eq.hashCode(e);
		return ret;
	}
	
	static <E> int commutativeHashCode(Enumerable<E> elems, Equalizer<? super E> eq) {
		int ret = 0; Objects.requireNonNull(eq);
		for(E e : elems) ret += eq.hashCode(e);
		return ret;
	}
	
	static <E> boolean equals(Enumerable<E> elems1, Enumerable<E> elems2) { return equals(elems1,elems2,Equalizer.natural()); }
	
	static <E> boolean equals(Enumerable<E> elems1, Enumerable<E> elems2, Equalizer<? super E> eq) {
		Objects.requireNonNull(eq);
		var etr1 = elems1.enumerate(); var etr2 = elems2.enumerate();
		while(true) {
			var nxt = etr1.next();
			if(nxt != etr2.next()) return false;
			if(!nxt) return true;
			if(!eq.equals(etr1.curr(),etr2.curr())) return false;
		}
	}
	
	static <E extends Comparable<? super E>> int compare(Enumerable<E> elems1, Enumerable<E> elems2) { return compare(elems1,elems2,Comparator.naturalOrder()); }
	
	static <E> int compare(Enumerable<E> elems1, Enumerable<E> elems2, Comparator<? super E> cmp) {
		Objects.requireNonNull(cmp);
		var etr1 = elems1.enumerate(); var etr2 = elems2.enumerate();
		while(true) {
			var nxt = etr1.next();
			if(nxt != etr2.next()) return nxt ? 1 : -1;
			if(!nxt) return 0;
			int c = cmp.compare(etr1.curr(),etr2.curr());
			if(c != 0) return c;
		}
	}
	
	static <E> String toString(Enumerable<E> elems) {
		var b = new StringBuilder("["); var first = true;
		for(var e : elems) {
			if(!first) b.append(", ");
			b.append(e); first = false;
		}
		return b.append("]").toString();
	}
	
//	copy
	
	@SuppressWarnings("deprecation")
	default Object[] toArray() { return ArrayList.toArray0(this); }
	
	@SuppressWarnings("deprecation")
	default <T> T[] toArray(T[] a) { return ArrayList.toArray0(this,a); }
	
	default <T> T[] toArray(IntFunction<T[]> suppl) { return toArray(suppl.apply(0)); }
	
	default List<E> toList() {
		var l = new ArrayList<E>();
		for(var e : this) l.add(e);
		return l;
	}
	
	default Collection<E> toSet() { return toSet(DuplicateAction.defaultAction,Equalizer.natural()); }
	
	default Collection<E> toSet(DuplicateAction duplicateAction, Equalizer<E> eq) {
		var ret = new HashSet<>(Objects.requireNonNull(eq));
		try(var elems = ret.enumerate()) { HelperMap.copy(this,elems,duplicateAction,e->e); }
		return ret;
	}
	
//	java iterator
	
	@Deprecated @Override default Iterator<E> iterator() { return JWrapper.wrap(enumerate()); }
	
	@Deprecated @Override default void forEach(Consumer<? super E> action) { throw new UnsupportedOperationException(); }
	
	@Deprecated @Override default Spliterator<E> spliterator() { throw new UnsupportedOperationException(); }
}
