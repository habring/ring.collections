package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.ConcurrentModificationException;

import ring.util.ArrayUtil;
import ring.util.IndexException;

class Segment<E> extends AbstractList<E> {
	private final List<E> l;
	private final int from;
	private int to;
	
	public Segment(List<E> l, int from, int to) {
		ArrayUtil.checkSubRange(l.size(),from,to);
		this.l = l; this.from = from; this.to = to;
	}
	
	@Override public Enumerator<E> enumerate() { return new SegmentEtr<>(this); }
	
	private static class SegmentEtr<E> implements Enumerator<E> {
		final Segment<E> owner;
		final Enumerator<E> elems;
		
		SegmentEtr(Segment<E> owner) { this.owner = owner; this.elems = owner.l.enumerate(owner.from-1); }
		
		void checkExists() { ArrayUtil.checkIndex(size(),index()); }
		
		@Override public E curr() { checkExists(); return elems.curr(); }
		
		@Override public int index() { return elems.index()-owner.from; }
		
		@Override public int size() { return owner.to-owner.from; }
		
		@Override public void jumpToStart() { elems.jumpTo(owner.from-1); }
		
		@Override public void jumpToEnd() { elems.jumpTo(owner.to); }
		
		@Override public void jumpTo(int i) { elems.jumpTo(owner.from + ArrayUtil.checkIndex(size(),i)); }
		
		@Override
		public boolean findFirst(E elem) {
			if(elems.findFirst(elem) && elems.index() < owner.to) return true;
			else { jumpToEnd(); return false; }
		}
		
		@Override
		public boolean next() {
			int i = elems.index();
			if(i < owner.to-1) {
				if(elems.next()) return true;
				throw new ConcurrentModificationException();
			}
			else if(i == owner.to) return false;
			else if(i == owner.to-1) { elems.next(); return false; }
			else throw new ConcurrentModificationException(); //i > owner.to
		}
		
		@Override
		public boolean prev() {
			int i = elems.index();
			if(i > owner.from) {
				if(elems.prev()) return true;
				throw new ConcurrentModificationException();
			}
			else if(i == owner.from-1) return false;
			else if(i == owner.from) { elems.prev(); return false; }
			else throw new ConcurrentModificationException(); //i < owner.from-1
		}
		
		@Override public E set(E elem) { checkExists(); return elems.set(elem); }
		
		void checkAddable() {
			if(elems.index() >= owner.to) throw new IndexException(-1,size(),index(),true,false);
		}
		
		@Override
		public void add(E elem) {
			checkAddable(); elems.add(elem); owner.to++;
		}
		
		@Override
		public int addAll(Collection<? extends E> c) {
			checkAddable(); int ret = elems.addAll(c); owner.to += ret; return ret;
		}
		
		@Override public E remove() { checkExists(); E ret = elems.remove(); owner.to--; return ret; }
		
		@Override public void flush() { elems.flush(); }
		
		@Override public void close() { elems.close(); }
	}
}
