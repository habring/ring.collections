package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

import ring.collections.HelperMap.*;
import ring.collections.impl.HashMap;
import ring.func.Equalizer;

public interface Map<K,V> extends Collection<Entry<K,V>> {
	Equalizer<K> keyEqualizer();
	
	default Enumerator<Entry<K,V>> enumerate(K key) {
		var elems = enumerate(); return elems.findFirst(new KeyEntry<>(key)) ? elems : null;
	}
	
	default boolean containsKey(K key) { return enumerate(key) != null; }
	
	default V get(K key) { return get(key,null); }
	
	default V get(K key, V defaultValue) { var elems = enumerate(key); return elems != null ? elems.curr().getValue() : defaultValue; }
	
	default V set(K key, V value) {
		try(var elems = enumerate(key)) {
			if(elems != null) return elems.curr().setValue(value);
			throw new NoSuchElementException(key.toString());
		}
	}
	
	default void add(K key, V value) { add(new SimpleEntry<>(key,value)); }
	
	default boolean addIfAbsent(K key, V value) {
		try(var elems = enumerate()) {
			if(elems.findFirst(new KeyEntry<>(key))) return false;
			elems.add(new SimpleEntry<>(key,value)); return true;
		}
	}
	
	default V setOrAdd(K key, V value) {
		try(var elems = enumerate()) {
			if(elems.findFirst(new KeyEntry<>(key))) return elems.curr().setValue(value);
			else { elems.add(new SimpleEntry<>(key,value)); return null; }
		}
	}
	
	default V removeKey(K key) {
		V ret = removeKeyIfPresent(key);
		if(ret == null) throw new NoSuchElementException();
		return ret;
	}
	
	default V removeKeyIfPresent(K key) {
		try(var elems = enumerate(key)) {
			return elems != null ? elems.remove().getValue() : null;
		}
	}
	
//	constructors
	
	@Override default Map<K,V> immutable() { return new ImmMap<>(this); }
	
	default Collection<K> keys() { return new KeyCollection<>(this); }
	
	default Collection<V> values() { return new ValueCollection<>(this); }
	
	static <K,V> Map<K,V> empty() { return EmptyMap.get(); }
	
	static <K,V> Map<K,V> of(K key, V value) { return of(key,value,Equalizer.natural()); }
	
	static <K,V> Map<K,V> of(K key, V value, Equalizer<K> eq) { return new SingletonMap<>(key,value,eq); }
	
//	functional
	
	default void forAll(BiConsumer<? super K,? super V> c) { Objects.requireNonNull(c); enumerate().forRest(e->c.accept(e.getKey(),e.getValue())); }
	
	default V compute(K key, BiFunction<? super K,? super V,? extends V> f) {
		try(var elems = enumerate()) {
			V value;
			if(elems.findFirst(new KeyEntry<>(key))) {
				var e = elems.curr();
				value = f.apply(e.getKey(),e.getValue());
				if(value == null) elems.remove();
				else elems.curr().setValue(value);
			}
			else {
				value = f.apply(key,null);
				if(value != null) elems.add(new SimpleEntry<>(key,value));
			}
			return value;
		}
	}
	
	default void replaceAll(BiFunction<? super K,? super V,? extends V> f) {
		Objects.requireNonNull(f);
		for(var e : this) {
			var value = f.apply(e.getKey(),e.getValue());
			e.setValue(Objects.requireNonNull(value));
		}
	}
	
//	mapping and filtering
	
	default <R> Map<K,R> mapValues(BiFunction<? super K,? super V,R> f) {
		Objects.requireNonNull(f); var out = this;
		return new AbstractMap<>() {
			@Override public Equalizer<K> keyEqualizer() { return out.keyEqualizer(); }
			@Override public Enumerator<Entry<K,R>> enumerate() { return new MappedMapEtr<>(out,f); }
		};
	}
	
	default <R> Map<K,R> mapValues(BiFunction<? super K,? super V,R> apply, BiFunction<? super K,? super R,V> revert) {
		Objects.requireNonNull(apply); Objects.requireNonNull(revert); var out = this;
		return new AbstractMap<>() {
			@Override public Equalizer<K> keyEqualizer() { return out.keyEqualizer(); }
			@Override public Enumerator<Entry<K,R>> enumerate() { return new BiMappedMapEtr<>(out,apply,revert); }
		};
	}
	
	default <A,B> Map<A,B> mapEntries(Function<Entry<K,V>,Entry<A,B>> apply, Function<Entry<A,B>,Entry<K,V>> revert) { return new KVMappedMap<>(this,apply,revert); }
	
//	copy
	
	default Map<K,V> toMap() { return new HashMap<>(this,keyEqualizer()); }
	
	static <K,V> Map<K,V> toMap(Enumerable<Entry<K,V>> elems) { return toMap(elems,DuplicateAction.defaultAction,Equalizer.natural()); }
	
	static <K,V> Map<K,V> toMap(Enumerable<Entry<K,V>> elems, DuplicateAction duplicateAction, Equalizer<K> eq) {
		Objects.requireNonNull(eq);
		var ret = new HashMap<K,V>(eq);
		try(var tmp = ret.enumerate()) { HelperMap.copy(elems,tmp,duplicateAction,e->new KeyEntry<>(e.getKey())); }
		return ret;
	}
}
