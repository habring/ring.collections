package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.io.Closeable;
import java.io.Flushable;
import java.util.Objects;
import java.util.function.*;

import ring.collections.HelperEtr.*;
import ring.func.IntBiConsumer;

public interface Enumerator<E> extends Flushable, Closeable {
	E curr();
	
	default int index() { throw new UnsupportedOperationException(); }
	
	default int size() { throw new UnsupportedOperationException(); }
	
	default void jumpToStart() { throw new UnsupportedOperationException(); }
	
	default void jumpToEnd() { throw new UnsupportedOperationException(); }
	
	default void jumpTo(int i) { throw new UnsupportedOperationException(); }
	
	default boolean findFirst(E elem) { return HelperEtr.defaultFindFirst(this,elem); }
	
	boolean next();
	
	default boolean prev() { throw new UnsupportedOperationException(); }
	
	/**Replaces the current element with the given one.*/
	default E set(E elem) { throw new UnsupportedOperationException(); }
	
	/**Adds the given element immediately after the current one and jumps to it.<br>
	 * If the underlying data structure is not a list, adds it anywhere and jumps to it.*/
	default void add(E elem) { throw new UnsupportedOperationException(); }
	
	/**Works like calling {@link #add(Object)} with all of the given elements (in the order provided).
	 * So this method jumps to the last added element.*/
	default int addAll(Collection<? extends E> c) {
		int num = 0;
		for(var e : c) { add(e); num++; }
		return num;
	}
	
	@SuppressWarnings("check that remove(), next(), prev() and findFirst() really do what they should do everywhere")
	/**Removes the current element.<br>
	 * A subsequent call to any method that refers to the current element is undefined.<br>
	 * A subsequent call to {@link #next()} or {@link #prev()} will do exactly the same as if the element would not have been removed.<br>
	 * If that is not possible (because of an internal mandatory reordering e.g. in a hash map), it jumps to the start.*/
	default E remove() { throw new UnsupportedOperationException(); }
	
	/**When you make a change to the underlying collection, it may be in an unstable state until you {@link #flush()} the changes.<br>
	 * An underlying {@link Enumerator} may choose to {@link #flush()} automatically at any time.*/
	@Override default void flush() {}
	
	@Override default void close() { flush(); }
	
//	constructors
	
	default Enumerator<E> immutable() { return new ImmEtr<>(this); }
	
	default Enumerator<E> inverse() { return new InvEtr<>(this); }
	
	static <E> Enumerator<E> empty() { return EmptyEtr.get(); }
	
//	functional
	
	default boolean findFirstMatch(Predicate<? super E> p) {
		Objects.requireNonNull(p);
		while(next()) {
			if(p.test(curr())) return true;
		}
		return false;
	}
	
	default void forRest(Consumer<? super E> c) {
		Objects.requireNonNull(c);
		while(next()) c.accept(curr());
	}
	
	default void forRestIndexed(IntBiConsumer<? super E> c) {
		Objects.requireNonNull(c); index();
		while(next()) c.accept(index(),curr());
	}
	
	default <R> R foldRest(R first, BiFunction<R,? super E,R> f) {
		Objects.requireNonNull(f);
		R ret = first;
		while(next()) ret = f.apply(ret,curr());
		return ret;
	}
	
	default <R> R foldRest(R first, BiFunction<R,? super E,R> f, Predicate<? super R> interrupt) {
		Objects.requireNonNull(f);
		R ret = first;
		while(!interrupt.test(ret) && next()) ret = f.apply(ret,curr());
		return ret;
	}
	
	default boolean testRest(Predicate<? super E> p) {
		Objects.requireNonNull(p);
		while(next()) {
			if(!p.test(curr())) return false;
		}
		return true;
	}
	
	default int countRest(Predicate<? super E> p) {
		Objects.requireNonNull(p);
		int n = 0;
		while(next()) n += p.test(curr()) ? 1 : 0;
		return n;
	}
	
	default void setRest(IntFunction<? extends E> f) {
		Objects.requireNonNull(f);
		while(next()) set(f.apply(index()));
	}
	
	default void replaceRest(Function<? super E, ? extends E> f) {
		Objects.requireNonNull(f);
		while(next()) set(f.apply(curr()));
	}
	
//	mapping and filtering
	
	default <R> Enumerator<R> map(Function<? super E,R> f) { return new MappedEtr<>(this,f); }
	
	default <R> Enumerator<R> map(Function<? super E,R> apply, Function<R,? extends E> revert) { return new BiMappedEtr<>(this,apply,revert); }
	
	default Enumerator<E> filter(Predicate<? super E> filter) { return new FilteredEtr<>(this,filter); }
	
	default <R> Enumerator<R> filterType(Class<R> type) { return new TypeFilteredEtr<>(this,type); }
	
	default Enumerator<E> asLongAs(Predicate<E> p) { return new AsLongAsEtr<>(this,p); }
}
