package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import ring.collections.HelperColl.EmptyList;
import ring.collections.HelperColl.ImmColl;
import ring.collections.HelperColl.InvColl;
import ring.collections.HelperColl.ZippedColl;
import ring.collections.HelperEtr.RepeatedEtr;
import ring.collections.impl.ArrayList;
import ring.collections.impl.HashSet;
import ring.func.Equalizer;

public interface Collection<E> extends Enumerable<E> {
	default void ensureCapacity(int minCapacity) {}
	
	default void trimToSize() {}
	
	/**The {@link Equalizer} in case this collection is a set.*/
	default Equalizer<E> equalizer() { return null; }
	
	default int size() { return enumerate().size(); }
	
	default boolean contains(E elem) { return enumerate().findFirst(elem); }
	
	default boolean containsAll(Collection<? extends E> c) {
		for(var e : c) {
			if(!contains(e)) return false;
		}
		return true;
	}
	
	default boolean containsAny(Predicate<? super E> p) { return anyMatch(p); }
	
	default void add(E elem) {
		try(var elems = enumerate()) { elems.add(elem); }
	}
	
	default boolean addIfAbsent(E elem) {
		try(var elems = enumerate()) {
			if(elems.findFirst(elem)) return false;
			elems.add(elem); return true;
		}
	}
	
	default int addAll(Collection<? extends E> c) {
		try(var elems = enumerate()) { return elems.addAll(c); }
	}
	
	default void remove(E elem) {
		try(var elems = enumerate()) {
			if(!elems.findFirst(elem)) throw new NoSuchElementException();
			elems.remove();
		}
	}
	
	default boolean removeIfPresent(E elem) {
		try(var elems = enumerate()) {
			if(elems.findFirst(elem)) { elems.remove(); return true; }
			else return false;
		}
	}
	
	default int removeAll(E elem) {
		try(var elems = enumerate()) {
			int num = 0;
			while(elems.findFirst(elem)) { elems.remove(); num++; }
			return num;
		}
	}
	
	default int removeAllIn(Collection<? super E> c) { return removeIf(Objects.requireNonNull(c)::contains); }
	
	default int retainAllIn(Collection<? super E> c) { Objects.requireNonNull(c); return removeIf(e -> !c.contains(e));  }
	
	default int removeIf(Predicate<? super E> p) {
		try(var elems = enumerate()) {
			int num = 0;
			while(elems.findFirstMatch(p)) { elems.remove(); num++; }
			return num;
		}
	}
	
	default void clear() {
		try(var elems = enumerate()) {
			while(elems.next()) elems.remove();
		}
	}
	
//	constructors
	
	@Override default Collection<E> immutable() { return new ImmColl<>(this); }
	
	@Override default Collection<E> inverse() { return new InvColl<>(this); }
	
	static <E> Collection<E> empty() { return EmptyList.get(); }
	
//	functional
	
	@Deprecated @Override default int count() { return size(); }
	
//	mapping
	
	@Override
	default <R> Collection<R> map(Function<? super E,R> f) {
		Objects.requireNonNull(f); var out = this;
		return new AbstractCollection<>() {
			@Override public Enumerator<R> enumerate() { return out.enumerate().map(f); }
		};
	}
	
	@Override
	default <R> Collection<R> map(Function<? super E,R> apply, Function<R,? extends E> revert) {
		Objects.requireNonNull(apply); Objects.requireNonNull(revert); var out = this;
		return new AbstractCollection<>() {
			@Override public Enumerator<R> enumerate() { return out.enumerate().map(apply,revert); }
		};
	}
	
//	zip and concat
	
	static <A,B> Collection<Entry<A,B>> zip(Collection<A> first, Collection<B> second) { return zip(first,second,HelperColl.defaultZipper()); }
	
	static <R,A,B> Collection<R> zip(Collection<A> first, Collection<B> second, BiFunction<A,B,R> combine) { return zip(first,second,combine,null,null); }
	
	static <R,A,B> Collection<R> zip(Collection<A> first, Collection<B> second, BiFunction<A,B,R> combine, Function<A,R> onlyFirst, Function<B,R> onlySecond) {
		return new ZippedColl<>(first,second,combine,onlyFirst,onlySecond);
	}
	
	static <E> boolean isRectangular(Collection<? extends Collection<E>> c) {
		var elems = c.enumerate();
		if(!elems.next()) return true;
		int s = elems.curr().size();
		while(elems.next()) {
			if(elems.curr().size() != s) return false;
		}
		return true;
	}
	
	@Override
	default Collection<E> repeat(int times) {
		var out = this;
		return new AbstractCollection<>() {
			@Override public Enumerator<E> enumerate() { return new RepeatedEtr<>(out.enumerate(),times); }
		};
	}
	
//	copy
	
	@SuppressWarnings("deprecation")
	@Override default Object[] toArray() { return ArrayList.toArray0(this); }
	
	@SuppressWarnings("deprecation")
	@Override default <T> T[] toArray(T[] a) { return ArrayList.toArray0(this,a); }
	
	@Override default <T> T[] toArray(IntFunction<T[]> suppl) { return toArray(suppl.apply(size())); }
	
	@Override default List<E> toList() { return new ArrayList<>(this); }
	
	@Override
	default Collection<E> toSet(DuplicateAction duplicateAction, Equalizer<E> eq) {
		Objects.requireNonNull(eq);
		if(eq.equals(equalizer()) || duplicateAction == DuplicateAction.error) return new HashSet<>(this,eq);
		else return Enumerable.super.toSet(duplicateAction,eq);
	}
}
