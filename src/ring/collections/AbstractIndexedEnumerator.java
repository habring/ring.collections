package ring.collections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import ring.util.ArrayUtil;

public abstract class AbstractIndexedEnumerator<E> implements Enumerator<E> {
	private int currIdx = -1;
	
	@Override public int index() { return currIdx; }
	
	@Override public abstract int size();
	
	@Override public void jumpToStart() { currIdx = -1; }
	
	@Override public void jumpToEnd() { currIdx = size(); }
	
	@Override public void jumpTo(int i) { currIdx = ArrayUtil.checkIndexEtr(size(),i); }
	
	@Override
	public boolean next() {
		if(currIdx < size()) currIdx++;
		return currIdx < size();
	}
	
	@Override
	public boolean prev() {
		if(currIdx >= 0) currIdx--;
		return currIdx >= 0;
	}
}