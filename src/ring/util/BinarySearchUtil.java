package ring.util;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Comparator;

import ring.collections.Enumerator;
import ring.collections.List;

public abstract class BinarySearchUtil<Index,Target> extends NoInstance {
	private static final ErrorMessages errors = new ErrorMessages();
	
	protected abstract int compare(Index a, Index b);
	
	protected abstract int compareWithTarget(Index i, Target t);
	
	protected abstract Index average(Index a, Index b);
	
	protected Index addUlp(Index i) { return i; }
	
	protected Index subtractUlp(Index i) { return i; }
	
	protected boolean doLinearSearch(Index from, Index to) { return false; }
	
	/**From and to are both inclusive.*/
	public final Index binarySearch(Target target, Index from, Index to) {
		if(doLinearSearch(from,to)) {
			if(addUlp(from) == from) throw new Error(errors.unreachable);
			while(compare(from,to) <= 0) {
				int cmp = compareWithTarget(from,target);
				if(cmp >= 0) return cmp == 0 ? from : null;
				from = addUlp(from);
			}
			return null;
		}
		else {
			while(compare(from,to) <= 0) {
				var mid = average(from,to);
				int c = compareWithTarget(mid,target);
				if(c < 0) from = addUlp(mid);
				else if(c > 0) to = subtractUlp(mid);
				else return mid;
			}
			return null;
		}
	}
	
	public static abstract class IntBinarySearchUtil<Target> extends BinarySearchUtil<Integer,Target> {
		private static final int linSearchThreshold = 10;
		
		@Override protected Integer average(Integer a, Integer b) { return (a+b)/2; }
		
		@Override protected Integer addUlp(Integer i) { return i+1; }
		
		@Override protected Integer subtractUlp(Integer i) { return i-1; }
		
		@Override protected boolean doLinearSearch(Integer from, Integer to) { return to-from <= linSearchThreshold; }
	}
	
	public static final class ListBinarySearchUtil<E> extends IntBinarySearchUtil<E> {
		private final Enumerator<E> elems;
		private final Comparator<? super E> cmp;
		
		@SuppressWarnings("unchecked")
		public ListBinarySearchUtil(List<E> l, Comparator<? super E> cmp) {
			this.elems = l.enumerate(); this.cmp = cmp != null ? cmp : (Comparator<E>)Comparator.naturalOrder();
		}
		
		@Override protected int compare(Integer a, Integer b) { return cmp.compare(get(a),get(b)); }
		
		@Override protected int compareWithTarget(Integer i, E elem) { return cmp.compare(get(i),elem); }
		
		private E get(int i) { elems.jumpTo(i); return elems.curr(); }
		
		public Integer binarySearch(E elem) { return binarySearch(elem,0,elems.size()); }
	}
}
