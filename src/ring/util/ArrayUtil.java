package ring.util;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

public final class ArrayUtil extends NoInstance {
    public static final int maxArrayLength = Integer.MAX_VALUE - 8;
    
	public static int checkIndex(int size, int i) {
		if(i < size && i >= 0) return i;
		throw new IndexException(size,i);
	}
	
	public static int checkIndexIncl(int size, int i) {
		if(i <= size && i >= 0) return i;
		throw new IndexException(size,i,true);
	}
	
	public static int checkIndexEtr(int size, int i) {
		if(i <= size && i >= -1) return i;
		throw new IndexException(-1,size,i,true,true);
	}
	
	public static void checkSubRange(int size, int from, int to) {
		checkIndex(size,from); checkIndexIncl(size,to);
		if(from > to) throw new IllegalArgumentException(String.format("from(%d) > to(%d)",from,to));
	}
	
	public static int checkSize(int size) {
		if(size >= 0) return size;
		throw new IllegalArgumentException("size < 0");
	}
	
	public static int newLength(int oldLength, int prefGrowth) { return newLength(oldLength,1,prefGrowth); }
	
	public static int newLength(int oldLength, int minGrowth, int prefGrowth) {
		if(oldLength < 0) throw new IllegalArgumentException("oldLength < 0");
		if(minGrowth <= 0) throw new IllegalArgumentException("minGrowth <= 0");
		int ret = oldLength + Math.max(minGrowth,prefGrowth);
		if(ret < 0 || ret > maxArrayLength) {
			ret = oldLength + minGrowth;
			if(ret >= 0 && ret <= maxArrayLength) return maxArrayLength;
			throw new OutOfMemoryError("requested array length too large");
		}
		return ret;
	}
}
