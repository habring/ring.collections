package ring.util;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

public final class IndexException extends IndexOutOfBoundsException {
	private static final long serialVersionUID = 1L;
	private final int from, to, i;
	
	public IndexException(int size, int i) { this(size,i,false); }
	
	public IndexException(int size, int i, boolean incl) { this(0,size,i); }
	
	public IndexException(int from, int to, int i) { this(from,to,i,true,false); }
	
	public IndexException(int from, int to, int i, boolean fromIncl, boolean toIncl) {
		super(String.format("index %d is out of bounds %s%d,%d%s",i,fromIncl?"[":"]",from,to,toIncl?"]":"["));
		this.from = from; this.to = to; this.i = i;
	}
	
	public int size() { return to-from; }
	
	public int index() { return i; }
	
	public int from() { return from; }
	
	public int to() { return to; }
}
