package ring.jcollections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

public abstract class AbstractBatchIterator<E, C extends AbstractBatchCollection<E>> implements BatchIterator<E> {
	protected final C owner;
	protected final boolean autoflush;
	
	public AbstractBatchIterator(C owner, boolean autoflush) {
		this.owner = owner; this.autoflush = autoflush;
	}
	
	protected void autoflush() { if(autoflush) flush(); }
	
	@Override public void close() { flush(); }
}
