package ring.jcollections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

public final class GapArrayList<E> extends AbstractList<E> implements RandomAccess, Cloneable {
	private static final ErrorMessages errors = new ErrorMessages();
	private static final int defaultCapacity = 10;
	private static final Object[] empty = {};
	private Object[] data;
	private int size, gapIdx, gapLen;
	
	public GapArrayList() { this.data = empty; }
	
	public GapArrayList(int initialCapacity) {
		if(initialCapacity < 0) throw new IllegalArgumentException("initial capacity < 0");
		this.data = initialCapacity == 0 ? empty : new Object[initialCapacity];
	}
	
	public GapArrayList(Collection<? extends E> c) { addAll(c); }
	
	private int gapTo() { return gapIdx+gapLen; }
	
	private int gapToEnd() { return size-gapIdx; }
	
	private int minGapLen() { return Math.min(defaultCapacity,size+1); }
	
	private int maxGapLen() { return Math.max(minGapLen(),Math.min(gapToEnd()*2,size/4)); }
	
	public void trimToSize() { realloc(size); }
	
	public void ensureCapacity(int minCapacity) {
		if(minCapacity > data.length) realloc(minCapacity);
	}
	
	@Override public int size() { return size; }
	
	private int checkIndex(int i) {
		if(i < size && i >= 0) return i;
		throw new IndexOutOfBoundsException(i);
	}
	
	private int resolveIdx(int i) { return checkIndex(i) < gapIdx ? i : i + gapLen; }
	
	@SuppressWarnings("unchecked")
	@Override public E get(int i) { return (E)data[resolveIdx(i)]; }
	
	@SuppressWarnings("unchecked")
	@Override public E set(int i, E elem) { int j = resolveIdx(i); E ret = (E)data[j]; data[j] = elem; return ret; }
	
	@Override
	public void add(int i, E elem) {
		int j = i == size ? size+gapLen : resolveIdx(i);
		if(j == gapTo()) j = gapIdx;
		else if(j >= gapLen-1 && moveGapWeight(j) > moveGapWeight(j-gapLen)) moveGap(j-gapLen);
		else moveGap(j);
		if(gapLen == 0) createGap(gapIdx,minGapLen());
		data[gapIdx] = elem; gapIdx++; gapLen--; size++; modCount++;
		if(gapLen == 0) removeGap();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public E remove(int i) {
		int j = resolveIdx(i); E ret = (E)data[j];
		data[j] = null;
		if(j >= gapLen && moveGapWeight(j+1) > moveGapWeight(j-gapLen)) moveGap(j-gapLen);
		else { moveGap(j+1); gapIdx--; }
		gapLen++; size--;
		if(gapLen > maxGapLen()) removeGap();
		modCount++;
		return ret;
	}
	
	@Override public void clear() { fillWithNull(); gapIdx = gapLen = size = 0; }
	
	private void fillWithNull() {
		Arrays.fill(data,0,gapIdx,null);
		Arrays.fill(data,gapTo(),size+gapLen,null);
	}
	
	private void createGap(int i, int len) {
		if(gapLen != 0 || len <= 0) throw new Error(errors.unreachable);
		if(data.length < size+len) grow(size+len);
		System.arraycopy(data,i,data,i+len,size-i);
		Arrays.fill(data,i,i+len,null);
		gapIdx = i; gapLen = len; modCount++;
	}
	
	/**Amount of elements that need to be written to move the gap.*/
	private int moveGapWeight(int toIdx) {
		if(gapLen == 0) return 0;
		if(toIdx < gapIdx) return (gapIdx-toIdx) + (Math.min(toIdx+gapLen,gapIdx)-toIdx);
		else if(toIdx >= size) return removeGapWeight();
		else if(toIdx > gapIdx) return (toIdx-gapIdx) + (toIdx+gapLen-Math.max(toIdx,gapTo()));
		else return 0;
	}
	
	private void moveGap(int toIdx) {
		if(gapLen != 0) {
			if(toIdx < gapIdx) {
				System.arraycopy(data,toIdx,data,toIdx+gapLen,gapIdx-toIdx);
				Arrays.fill(data,toIdx,Math.min(toIdx+gapLen,gapIdx),null);
			}
			else if(toIdx >= size) { removeGap(); toIdx = gapIdx; }
			else if(toIdx > gapIdx) {
				System.arraycopy(data,gapTo(),data,gapIdx,toIdx-gapIdx);
				Arrays.fill(data,Math.max(toIdx,gapTo()),toIdx+gapLen,null);
			}
		}
		gapIdx = toIdx;
	}
	
	private int removeGapWeight() { return gapToEnd()+gapLen; }
	
	private void removeGap() {
		if(gapLen == 0) { gapIdx = size; return; }
		System.arraycopy(data,gapTo(),data,gapIdx,gapToEnd());
		Arrays.fill(data,size,size+gapLen,null);
		gapIdx = size; gapLen = 0; modCount++;
	}
	
	private void grow(int minCapacity) { realloc(Math.max(defaultCapacity,minCapacity+minCapacity/2)); }
	
	private void realloc(int capacity) {
		if(capacity < size) throw new Error(errors.unreachable);
		if(capacity != data.length) {
			var data = new Object[capacity];
			System.arraycopy(this.data,0,data,0,gapIdx);
			System.arraycopy(this.data,gapTo(),data,gapIdx,gapToEnd());
			fillWithNull(); this.data = data; gapIdx = size; gapLen = 0; modCount++;
		}
	}
}
