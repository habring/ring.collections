package ring.jcollections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.*;
import java.util.AbstractCollection;
import java.util.AbstractList;
import java.util.AbstractMap;

import ring.collections.Collection;
import ring.collections.Enumerable;
import ring.collections.Enumerator;
import ring.collections.KeyEntry;
import ring.collections.List;
import ring.collections.Map;
import ring.collections.Queue;
import ring.func.Equalizer;
import ring.util.NoInstance;

public final class JWrapper extends NoInstance {
	public static <E> java.util.Iterator<E> wrap(Enumerator<E> elems) {
		Objects.requireNonNull(elems);
		return new Iterator<>() {
			boolean hasNext;
			@Override public boolean hasNext() { return hasNext || (hasNext = elems.next()); }
			@Override
			public E next() {
				if(!hasNext()) throw new NoSuchElementException();
				hasNext = false; return elems.curr();
			}
			@Override public void remove() { elems.remove(); elems.flush(); }
		};
	}
	
	public static <E> java.lang.Iterable<E> wrap(Enumerable<E> elems) {
		Objects.requireNonNull(elems); return () -> wrap(elems.enumerate());
	}
	
	@SuppressWarnings("deprecation")
	public static <E> java.util.Collection<E> wrap(Collection<E> c) {
		Objects.requireNonNull(c);
		return new AbstractCollection<>() {
			@Override public int size() { return c.size(); }
			@Override public Iterator<E> iterator() { return c.iterator(); }
			@Override
			public boolean add(E e) {
				if(c.equalizer() != null) return c.addIfAbsent(e);
				else { c.add(e); return true; }
			}
		};
	}
	
	@SuppressWarnings("deprecation")
	public static <E> java.util.Queue<E> wrap(Queue<E> q) {
		Objects.requireNonNull(q);
		return new AbstractQueue<>() {
			@Override public int size() { return q.size(); }
			@Override public Iterator<E> iterator() { return q.iterator(); }
			@Override public boolean offer(E e) { q.add(e); return true; }
			@Override public E poll() { return q.removeHead(); }
			@Override
			public E peek() {
				try(var elems = q.enumerate()) {
					return elems.next() ? elems.curr() : null;
				}
			}
		};
	}
	
	public static <E> java.util.List<E> wrap(List<E> l) {
		Objects.requireNonNull(l);
		return new AbstractList<>() {
			@Override public int size() { return l.size(); }
			@Override public E get(int i) { return l.get(i); }
			@Override public E set(int i, E elem) { return l.set(i,elem); }
			@Override public void add(int i, E elem) { l.add(i,elem); }
			@Override public E remove(int i) { return l.removeAt(i); }
		};
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public static <K,V> java.util.Map<K,V> wrap(Map<K,V> m) {
		Objects.requireNonNull(m); var entries = wrapSet(m);
		return new AbstractMap<>() {
			@Override public Set<Entry<K,V>> entrySet() { return entries; }
			@Override
			public V put(K key, V value) {
				try(var elems = m.enumerate()) {
					if(elems.findFirst(new KeyEntry<>(key))) {
						return elems.curr().setValue(value);
					}
					else { elems.add(new SimpleEntry<>(key,value)); return null; }
				}
			}
			@Override public boolean containsKey(Object key) { return m.containsKey((K)key); }
			@Override public V get(Object key) { return m.get((K)key); }
			@Override public V remove(Object key) { return m.removeKeyIfPresent((K)key); }
		};
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public static <E> java.util.Set<E> wrapSet(Collection<E> c) {
		Objects.requireNonNull(c);
		return new AbstractSet<>() {
			@Override public int size() { return c.size(); }
			@Override public Iterator<E> iterator() { return c.iterator(); }
			@Override public boolean add(E e) { return c.addIfAbsent(e); }
			@Override public boolean contains(Object o) { return c.contains((E)o); }
			@Override public boolean remove(Object o) { return c.removeIfPresent((E)o); }
		};
	}
	
	/**@see Enumerable#equals(Enumerable, Enumerable)*/
	public static <E> boolean equals(Iterable<E> it1, Iterable<E> it2) { return equals(it1,it2,Equalizer.natural()); }
	
	/**@see Enumerable#equals(Enumerable, Enumerable)*/
	public static <E> boolean equals(Iterable<E> it1, Iterable<E> it2, Equalizer<? super E> eq) {
		Objects.requireNonNull(eq);
		var x = it1.iterator(); var y = it2.iterator();
		while(true) {
			var nxt = x.hasNext();
			if(nxt != y.hasNext()) return false;
			if(!nxt) return true;
			E e1 = x.next(), e2 = y.next();
			if(!eq.equals(e1,e2)) return false;
		}
	}
}
