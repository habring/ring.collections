package ring.jcollections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.io.Closeable;
import java.io.Flushable;
import java.util.Collection;
import java.util.Iterator;

public interface BatchIterator<E> extends Iterator<E>, Flushable, Closeable {
	/**Works like a {@link Collection#add(Object)}, but with batch support.*/
	boolean append(E e);
	
	/**Works like a {@link Collection#addAll(Collection)}, but with batch support.*/
	boolean appendAll(Collection<? extends E> c);
	
	/**When you make a change to the underlying {@link BatchCollection}, it may be in an unstable state until you {@link #flush()} the changes.<br>
	 * An underlying {@link BatchIterator} may choose to {@link #flush()} automatically at any time.*/
	@Override void flush();
	
	@Override default void close() { flush(); }
}
