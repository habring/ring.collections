package ring.jcollections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.*;
import java.util.function.UnaryOperator;

import ring.util.ArrayUtil;
import ring.util.IndexException;

public abstract class AbstractBatchList<E> extends AbstractBatchCollection<E> implements BatchList<E> {
	protected transient int modCount = 0;
	
	@Override public E get(int i) { return listIterator(i).next(); }
	
	@Override
	public E set(int i, E elem) {
		var it = listIterator(i);
		E ret = it.next();
		it.set(elem); return ret;
	}
	
	@Override
	public int indexOf(Object o) {
		var it = listIterator();
		if(o == null) {
			while(it.hasNext()) {
				if(it.next() == null) return it.previousIndex();
			}
		}
		else {
			while(it.hasNext()) {
				if(o.equals(it.next())) return it.previousIndex();
			}
		}
		return -1;
	}
	
	@Override
	public int lastIndexOf(Object o) {
		var it = listIterator(size());
		if(o == null) {
			while(it.hasPrevious()) {
				if(it.previous() == null) return it.nextIndex();
			}
		}
		else {
			while(it.hasPrevious()) {
				if(o.equals(it.previous())) return it.nextIndex();
			}
		}
		return -1;
	}
	
	@Override public void add(int i, E elem) { listIterator(i).add(elem); }
	
	@Override
	public E remove(int i) {
		var it = listIterator(i);
		var ret = it.next(); it.remove(); return ret;
	}
	
	@Override public void clear() { removeRange(0,size()); }
	
	public void removeRange(int from, int to) {
		ArrayUtil.checkIndexIncl(size(),to);
		try(var it = batchListIterator(from)) {
			for(int i = from; i < to; i++) {
				it.next(); it.remove();
			}
		}
	}
	
	@Override
	public boolean addAll(int i, Collection<? extends E> c) {
		try(var it = batchListIterator(i)) { return it.addAll(c); }
	}
	
	@Override
	public void replaceAll(UnaryOperator<E> op) {
		Objects.requireNonNull(op);
		try(var it = batchListIterator()) {
			while(it.hasNext()) {
				it.set(op.apply(it.next()));
			}
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void sort(Comparator<? super E> c) {
		var a = toArray();
		Arrays.sort(a,(Comparator)c);
		try(var it = batchListIterator()) {
			for(var e : a) { it.next(); it.set((E)e); }
		}
	}
	
	@Override public Iterator<E> iterator() { return listIterator(); }
	
	@Override public ListIterator<E> listIterator() { return listIterator(0); }
	
	@Override public BatchIterator<E> batchIterator() { return batchListIterator(); }
	
	@Override public BatchListIterator<E> batchListIterator() { return batchListIterator(0); }
	
	@Override
	public BatchList<E> subList(int from, int to) {
		return this instanceof RandomAccess ? new RandomAccessSubList<>(this,from,to) : new SubList<>(this,from,to);
	}
	
	@Override
	public int hashCode() {
		int ret = 1;
		for(E e : this) ret = 31*ret + Objects.hashCode(e);
		return ret;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof List<?> l)) return false;
		if(o == this) return true;
		var it1 = listIterator(); var it2 = l.listIterator();
		while(it1.hasNext() && it2.hasNext()) {
			var e1 = it1.next(); var e2 = it2.next();
			if(!Objects.equals(e1,e2)) return false;
		}
		return !it1.hasNext() && !it2.hasNext();
	}
	
	private static class SubList<E> extends AbstractBatchList<E> {
		final AbstractBatchList<E> root;
		final SubList<E> parent;
		final int from; int size;
		
		SubList(AbstractBatchList<E> root, int from, int to) {
			this.root = Objects.requireNonNull(root); this.parent = null;
			this.from = from; this.size = to-from; init();
		}
		
		SubList(SubList<E> parent, int from, int to) {
			this.root = parent.root; this.parent = parent;
			this.from = parent.from+from; this.size = to-from; init();
		}
		
		void init() {
			this.modCount = root.modCount;
			if(parent != null) ArrayUtil.checkSubRange(parent.size,from-parent.from,from-parent.from+size);
			else ArrayUtil.checkSubRange(root.size(),from,from+size);
		}
		
		@Override public int size() { checkMod(); return size; }
		
		@Override public ListIterator<E> listIterator(int i) { return new SubItr<>(this,i,true); }
		
		@Override public BatchListIterator<E> batchListIterator(int i) { return new SubItr<>(this,i,false); }

		@Override public BatchList<E> subList(int from, int to) { return new SubList<>(this,from,to); }
		
		void checkMod() {
			if(root.modCount != modCount) throw new ConcurrentModificationException();
		}
		
		void changeSize(int diff) {
			for(var l = this; l != null; l = l.parent) {
				l.size += diff; l.modCount = root.modCount;
			}
		}
	}
	
	private static class RandomAccessSubList<E> extends SubList<E> implements RandomAccess {
		RandomAccessSubList(AbstractBatchList<E> root, int from, int to) { super(root,from,to); }
		
		RandomAccessSubList(RandomAccessSubList<E> parent, int from, int to) { super(parent,from,to); }
		
		@Override public BatchList<E> subList(int from, int to) { return new RandomAccessSubList<>(this,from,to); }
	}
	
	private static class SubItr<E> extends AbstractBatchIterator<E,SubList<E>> implements BatchListIterator<E> {
		final BatchListIterator<E> it;
		int storedNextIndex = -1;
		
		SubItr(SubList<E> owner, int i, boolean autoflush) {
			super(owner,autoflush);
			ArrayUtil.checkIndexIncl(owner.size,i);
			this.it = owner.root.batchListIterator(owner.from+i);
			owner.checkMod();
		}
		
		void appendMode() {
			if(storedNextIndex >= 0) return;
			storedNextIndex = nextIndex0();
			while(hasNext0()) it.next();
		}
		
		void normalMode() {
			if(storedNextIndex < 0) return;
			while(nextIndex0() > storedNextIndex) it.previous();
			storedNextIndex = -1;
		}
		
		@Override public boolean hasPrevious() { normalMode(); return previousIndex() >= 0; }
		
		@Override
		public E previous() {
			owner.checkMod();
			if(hasPrevious()) return it.previous();
			else throw new IndexException(owner.size,previousIndex());
		}
		
		@Override public int previousIndex() { normalMode(); return it.previousIndex() - owner.from; }
		
		@Override public boolean hasNext() { normalMode(); return hasNext0(); }
		
		private boolean hasNext0() { return nextIndex() < owner.size; }
		
		@Override
		public E next() {
			owner.checkMod();
			if(hasNext()) return it.next();
			else throw new IndexException(owner.size,nextIndex());
		}
		
		@Override public int nextIndex() { normalMode(); return nextIndex0(); }
		
		private int nextIndex0() { return it.nextIndex() - owner.from; }
		
		@Override public void set(E e) { normalMode(); owner.checkMod(); it.set(e); owner.changeSize(0); autoflush(); }
		
		@Override public void add(E e) { normalMode(); owner.checkMod(); it.add(e); owner.changeSize(1); autoflush(); }
		
		@Override public boolean addAll(Collection<? extends E> c) { normalMode(); owner.checkMod(); var ret = it.addAll(c); owner.changeSize(c.size()); autoflush(); return ret; }
		
		@Override public boolean append(E e) { appendMode(); owner.checkMod(); it.add(e); owner.changeSize(1); autoflush(); return true; }
		
		@Override public boolean appendAll(Collection<? extends E> c) { appendMode(); owner.checkMod(); it.addAll(c); owner.changeSize(c.size()); autoflush(); return c.size() > 0; }
		
		@Override public void remove() { normalMode(); owner.checkMod(); it.remove(); owner.changeSize(-1); autoflush(); }
		
		@Override public void flush() { it.flush(); }
		
		@Override public void close() { it.close(); }
	}
}
