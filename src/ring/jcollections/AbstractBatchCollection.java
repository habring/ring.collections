package ring.jcollections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import ring.util.ArrayUtil;

public abstract class AbstractBatchCollection<E> implements BatchCollection<E> {
	static final ErrorMessages errors = new ErrorMessages();
	
	@Override public boolean isEmpty() { return size() == 0; }
	
	@Override
	public boolean contains(Object o) {
		if(o == null) {
			for(E e : this) {
				if(e == null) return true;
			}
		}
		else {
			for(E e : this) {
				if(o.equals(e)) return true;
			}
		}
		return false;
	}
	
	@Override public Object[] toArray() { return copyTo(new Object[size()]); }
	
	@Override public <T> T[] toArray(T[] a) { return copyTo(a); }
	
	@Override public <T> T[] toArray(IntFunction<T[]> generator) { return toArray(generator.apply(size())); }
	
	@SuppressWarnings("unchecked")
	private <T> T[] copyTo(T[] dest) {
		int size = size();
		if(dest.length < size) dest = (T[])Array.newInstance(dest.getClass().getComponentType(),size);
		var it = iterator();
		int i = 0;
		for(; it.hasNext(); i++) {
			var e = it.next();
			if(i >= dest.length) {
				dest = Arrays.copyOf(dest,ArrayUtil.newLength(dest.length,dest.length/2+1));
				if(i >= dest.length) throw new Error(errors.unreachable);
				
			}
			dest[i] = (T)e;
		}
		if(i > dest.length) throw new Error(errors.unreachable);
		return i < dest.length ? Arrays.copyOf(dest,i) : dest;
	}
	
	@Override
	public boolean add(E e) {
		try(var it = batchIterator()) { return it.append(e); }
	}
	
	@Override
	public boolean remove(Object o) {
		var it = iterator();
		if(o == null) {
			while(it.hasNext()) {
				if(it.next() == null) { it.remove(); return true; }
			}
		}
		else {
			while(it.hasNext()) {
				if(o.equals(it.next())) { it.remove(); return true; }
			}
		}
		return false;
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		for(var e : c) {
			if(!contains(e)) return false;
		}
		return true;
	}
	
	@Override
	public boolean addAll(Collection<? extends E> c) {
		try(var it = batchIterator()) { return it.appendAll(c); }
	}
	
	@Override
	public boolean removeAll(Collection<?> c) {
		Objects.requireNonNull(c);
		var ret = false;
		try(var it = batchIterator()) {
			while(it.hasNext()) {
				if(c.contains(it.next())) { it.remove(); ret = true; }
			}
		}
		return ret;
	}
	
	@Override
	public boolean removeIf(Predicate<? super E> filter) {
		Objects.requireNonNull(filter);
		var ret = false;
		try(var it = batchIterator()) {
			while(it.hasNext()) {
				if(filter.test(it.next())) { it.remove(); ret = true; }
			}
		}
		return ret;
	}
	
	@Override
	public boolean retainAll(Collection<?> c) {
		Objects.requireNonNull(c);
		var ret = false;
		try(var it = batchIterator()) {
			while(it.hasNext()) {
				if(!c.contains(it.next())) { it.remove(); ret = true; }
			}
		}
		return ret;
	}
	
	@Override
	public void clear() {
		try(var it = batchIterator()) {
			while(it.hasNext()) { it.next(); it.remove(); }
		}
	}
	
	@Override public String toString() { return Arrays.toString(toArray()); }
}
