package ring.jcollections;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.*;

import ring.util.ArrayUtil;
import ring.util.IndexException;

public final class BatchArrayList<E> extends AbstractBatchList<E> implements RandomAccess, Cloneable {
	private static final int minCapacity = 10;
	private static final Object[] empty = {};
	private Object[] data;
	private int size;
	
	public BatchArrayList() { this.data = empty; }
	
	public BatchArrayList(int initialCapacity) { this(); ensureCapacity(initialCapacity); }
	
	public BatchArrayList(Collection<? extends E> c) { this(c.size()); addAll(c); }
	
	public void ensureCapacity(int minCapacity) {
		if(data.length >= minCapacity) return;
		modCount++;
		minCapacity = Math.max(minCapacity,BatchArrayList.minCapacity);
		var tmp = new Object[minCapacity];
		System.arraycopy(data,0,tmp,0,size);
		Arrays.fill(data,0,size,null);
		data = tmp;
	}
	
	private void grow(int growBy) {
		if(size+growBy > data.length) ensureCapacity(size+size/2+growBy);
	}
	
	public void trimToSize() {
		if(data.length == size) return;
		modCount++;
		if(size == 0) {
			Arrays.fill(data,0,size,null);
			data = empty;
		}
		else {
			var tmp = Arrays.copyOf(data,size);
			Arrays.fill(data,0,size,null);
			data = tmp;
		}
	}
	
	@Override public int size() { return size; }
	
	@Override public ListIterator<E> listIterator(int i) { return new Itr<>(this,i,true); }
	
	@Override public BatchListIterator<E> batchListIterator(int i) { return new Itr<>(this,i,false); }
	
	private static class Itr<E> extends AbstractBatchIterator<E,BatchArrayList<E>> implements BatchListIterator<E> {
		int nextIdx, lastRet = -1;
		int gapFrom, gapLen;
		int modCount;
		
		Itr(BatchArrayList<E> owner, int i, boolean autoflush) {
			super(owner,autoflush);
			this.nextIdx = ArrayUtil.checkIndexIncl(owner.size,i);
			resetModCnt(); flush();
		}
		
		void checkModCnt() {
			if(modCount != owner.modCount) throw new ConcurrentModificationException();
		}
		
		void resetModCnt() { modCount = owner.modCount; }
		
		void incModCnt() { owner.modCount++; resetModCnt(); }
		
		int gapTo() { return gapFrom+gapLen; }
		
		int internalIdx() { return internalIdx(nextIdx); }
		
		int internalIdx(int i) { return i < gapFrom ? i : i+gapLen; }
		
		@Override public boolean hasPrevious() { return nextIdx != 0; }
		
		@SuppressWarnings("unchecked")
		@Override
		public E previous() {
			checkModCnt();
			if(!hasPrevious()) throw new IndexException(owner.size,nextIdx-1);
			nextIdx--; lastRet = nextIdx; return (E)owner.data[internalIdx()];
		}
		
		@Override public int previousIndex() { return nextIdx-1; }
		
		@Override public boolean hasNext() { return nextIdx != owner.size; }
		
		@SuppressWarnings("unchecked")
		@Override
		public E next() {
			checkModCnt();
			if(!hasNext()) throw new IndexException(owner.size,nextIdx);
			E ret = (E)owner.data[internalIdx()]; lastRet = nextIdx; nextIdx++; return ret;
		}
		
		@Override public int nextIndex() { return nextIdx; }
		
		@Override
		public void set(E e) {
			if(lastRet < 0) throw new IllegalStateException();
			checkModCnt(); owner.data[internalIdx(lastRet)] = e;
			autoflush();
		}
		
		@Override
		public void add(E e) {
			flush(); owner.grow(1); resetModCnt();
			System.arraycopy(owner.data,nextIdx,owner.data,nextIdx+1,owner.size-nextIdx);
			owner.data[nextIdx] = e; owner.size++;
			nextIdx++; lastRet = -1; incModCnt();
			autoflush();
		}
		
		@Override
		public boolean addAll(Collection<? extends E> c) {
			var s = c.size();
			flush(); owner.grow(s); resetModCnt();
			System.arraycopy(owner.data,nextIdx,owner.data,nextIdx+s,owner.size-nextIdx);
			var it = c.iterator();
			for(int i = 0; i < s; i++) {
				if(it.hasNext()) owner.data[nextIdx+i] = it.next();
				else { gapFrom = nextIdx+i; gapLen = s-i; s = i; } //less elements than expected
			}
			owner.size += s; nextIdx += s; lastRet = -1; incModCnt();
			while(it.hasNext()) add(it.next()); //more elements than expected
			autoflush(); return s > 0;
		}
		
		@Override public boolean append(E e) { int i = nextIdx; nextIdx = owner.size; add(e); nextIdx = i; return true; }
		
		@Override public boolean appendAll(Collection<? extends E> c) { int i = nextIdx; nextIdx = owner.size; addAll(c); nextIdx = i; return c.size() > 0; }
		
		@Override
		public void remove() {
			if(lastRet < 0) throw new IllegalStateException();
			checkModCnt();
			if(lastRet < nextIdx) nextIdx--;
			if(gapLen == 0) { gapFrom = lastRet; gapLen = 1; }
			else if(lastRet < gapFrom) {
				int len = gapFrom-lastRet-1;
				System.arraycopy(owner.data,lastRet+1,owner.data,gapTo()-len,len);
				gapFrom = lastRet; gapLen++;
			}
			else {
				lastRet += gapLen; int len = lastRet-gapTo();
				System.arraycopy(owner.data,gapTo(),owner.data,gapFrom,len);
				gapFrom += len; gapLen++;
			}
			owner.data[lastRet] = null; owner.size--; lastRet = -1;
			incModCnt(); autoflush();
		}
		
		@Override
		public void flush() {
			checkModCnt();
			if(gapLen > 0) {
				System.arraycopy(owner.data,gapTo(),owner.data,gapFrom,owner.size+gapLen-gapTo());
				if(nextIdx >= gapTo()) nextIdx -= gapLen;
				lastRet = -1; incModCnt();
			}
			gapFrom = Integer.MAX_VALUE; gapLen = 0;
		}
	}
}
