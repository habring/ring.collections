package ring.func;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Objects;
import java.util.function.Function;

class Helper {
	static final Equalizer<Object> naturalEq = new Equalizer<>() {
		@Override public int hashCode(Object obj) { return Objects.hashCode(obj); }
		@Override public boolean equals(Object obj1, Object obj2) { return Objects.equals(obj1,obj2); }
	};
	
	static final Equalizer<Object> referenceEq = new Equalizer<>() {
		@Override public int hashCode(Object obj) { return System.identityHashCode(obj); }
		@Override public boolean equals(Object obj1, Object obj2) { return obj1 == obj2; }
	};
	
	static class MappedEqualizer<T,I> implements Equalizer<T> {
		final Equalizer<I> eq;
		final Function<? super T,? extends I> f;
		
		MappedEqualizer(Equalizer<I> eq, Function<? super T,? extends I> f) { this.eq = Objects.requireNonNull(eq); this.f = Objects.requireNonNull(f); }
		
		@Override public int hashCode(T obj) { return eq.hashCode(f.apply(obj)); }
		
		@Override public boolean equals(T obj1, T obj2) { return eq.equals(f.apply(obj1),f.apply(obj2)); }
	}
}
