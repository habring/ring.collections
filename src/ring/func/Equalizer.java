package ring.func;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.function.Function;

import ring.func.Helper.MappedEqualizer;

public interface Equalizer<T> {
	int hashCode(T obj);
	
	boolean equals(T obj1, T obj2);
	
	@SuppressWarnings("unchecked")
	static <T> Equalizer<T> natural() { return (Equalizer<T>)Helper.naturalEq; }
	
	@SuppressWarnings("unchecked")
	static <T> Equalizer<T> reference() { return (Equalizer<T>)Helper.referenceEq; }
	
	default <R> Equalizer<R> map(Function<R,? extends T> f) { return new MappedEqualizer<>(this,f); }
}
