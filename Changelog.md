# Ring.Serializer Changelog

# 21.01

- add CCLinkedQueue, EnumSet and CowArrayList

# 20.08

- Initial Release

