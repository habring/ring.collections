# Ring.Collections

This library provides a similar flexibility to Java as System.Linq does in C#.  
There are new Enumerable-, Collection-, List- and Map-interfaces and some default implementations designed from scratch.  
They allow you to map, filter, fold, zip and so on.  
When designing these new interfaces, also performance was a huge aspect.  
The goal was to design it in a way that the best performance is possible with the
least amount of effort when implementing new collection classes.  
The result is that e.g. the provided ArrayList- and HashMap-classes have around 1/10 (!) the amount of code
compared to their equivalents in the Java standard library.  
In worst case, both classes mentioned above have a performance overhead of around 50% compared to
the Java standard library, which is pretty decent, when you compare the implementation- and maintenance-cost
of having 10 times the amount of code on the other hand.  
In best case (due 2020-08), the provided classes perform even better than the ones in the Java standard library.
E.g. ArrayList.removeIf is implemented in a more efficient way.

# Compatibility

The Enumerable-interface is compatible with java.lang.Iterable. So foreach-loops are possible with the new collection types.  
There is also a class called JWrapper, which provides some basic wrapping for the new collection classes to make them
compatible with the Java standard library. This is however only recommended for testing purposes, because of performance.  
All classes other than JWrapper in the ring.jcollections-package should be seen as experimental. I implemented them before
I redesigned all of the Collection-interfaces and they may contain bugs which I may or may not decide to fix.

# Dependencies

Ring.Collections has no dependencies.

# Copyright

Copyright 2020 Lukas Habring

For commercial licenses, just contact Lukas Habring: <ring.commercial@gmx.at>  
This file is part of Ring.Collections.

Ring.Collections is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License version 3  
(and only version 3) as published by the Free Software Foundation.

Ring.Collections is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the  
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License  
along with Ring.Collections. If not, see <http://www.gnu.org/licenses/>.
