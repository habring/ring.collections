package ring.collections.impl;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import static ring.collections.impl.ListTest.*;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import ring.jcollections.JWrapper;

public class CCQueueTest {
	static final int numThreads = 8;
	
	public static void main(String[] args) {
		test(JWrapper.wrap(new ring.collections.impl.CCLinkedQueue<>()),testRuns,testRuns*benchmarkFactor);
		benchmark(new ConcurrentLinkedQueue<>(),CCQueueTest::randomAction,testRuns*benchmarkFactor);
		parallelBenchmark(JWrapper.wrap(new ring.collections.impl.CCLinkedQueue<>()),testRuns);
		parallelBenchmark(new ConcurrentLinkedQueue<>(),testRuns);
	}
	
	private static void test(Queue<Integer> q, int testRuns, int benchmarkRuns) {
		randomTest(q,new ConcurrentLinkedQueue<>(),testRuns);
		q.clear();
		if(q.size() != 0) throw new Error(errors.unreachable);
		benchmark(q,CCQueueTest::randomAction,benchmarkRuns);
	}
	
	private static void randomTest(Queue<Integer> q1, Queue<Integer> q2, int runs) {
		var rand1 = new Random(seed); var rand2 = new Random(seed);
		for(int i = 0; i < runs; i++) {
			randomAction(q1,rand1); randomAction(q2,rand2);
			if(!JWrapper.equals(q1,q2)) {
				System.out.printf("run: %d\n%s\n%s\n",i,q1,q2);
				throw new Error(errors.unreachable);
			}
		}
	}
	
	private static void randomAction(Queue<Integer> q, Random rand) {
		if(q.size() >= maxSize) q.clear();
		var c = rand.nextDouble() * 100;
		if(c <= 60 || q.isEmpty()) q.offer(rand.nextInt(maxValue));
		else q.poll();
	}
	
	private static void parallelBenchmark(Queue<Entry<Integer,Integer>> q, int testRuns) {
		System.out.println(q.getClass().getName()+":");
		q.clear();
		
		var time = System.nanoTime();
		var threads = new ArrayList<Thread>();
		var running = new AtomicInteger(numThreads);
		for(int i = 0; i < numThreads; i++) {
			int i_ = i;
			threads.add(new Thread(() -> {
				for(int j = 0; j < testRuns; j++) q.add(new SimpleEntry<>(i_,j));
				running.decrementAndGet();
			}));
			threads.add(new Thread(() -> {
				int total = 0;
				while(true) {
					var e = q.poll();
					if(e == null) {
						if(running.get() == 0) break;
						Thread.yield(); continue;
					}
					//System.out.printf("p%d->c%d: %d\n",e.getKey(),i_,e.getValue());
					total++;
				}
				System.out.printf("c%d: %d total\n",i_,total);
			}));
		}
		for(var t : threads) t.start();
		try {
			for(var t : threads) t.join();
		} catch(InterruptedException exc) { exc.printStackTrace(); }
		if(q.poll() != null) throw new IllegalStateException();
		time = System.nanoTime() - time;
		System.out.printf("runtime: %.2f ms\n\n",time/1000000D);
	}
}
