package ring.collections.impl;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import static ring.collections.impl.ListTest.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import ring.jcollections.JWrapper;

public class MapTest {
	public static void main(String[] args) {
		var m = new ring.collections.impl.HashMap<Integer,Integer>();
		test(JWrapper.wrap(m),testRuns,testRuns*benchmarkFactor);
		benchmark(new HashMap<>(),MapTest::randomAction,testRuns*benchmarkFactor);
		printBucketStatistics(m);
	}
	
	private static void test(Map<Integer,Integer> m, int testRuns, int benchmarkRuns) {
		randomTest(m,new HashMap<>(),testRuns);
		m.clear();
		if(m.size() != 0) throw new Error(errors.unreachable);
		benchmark(m,MapTest::randomAction,benchmarkRuns);
	}
	
	private static void randomTest(Map<Integer,Integer> m1, Map<Integer,Integer> m2, int runs) {
		var rand1 = new Random(seed); var rand2 = new Random(seed);
		for(int i = 0; i < runs; i++) {
			randomAction(m1,rand1); randomAction(m2,rand2);
			if(!m1.equals(m2)) {
				System.out.printf("run: %d\n%s\n%s\n",i,m1,m2);
				throw new Error(errors.unreachable);
			}
		}
	}
	
	private static void randomAction(Map<Integer,Integer> m, Random rand) {
		if(m.size() >= maxSize) m.clear();
		var c = rand.nextDouble() * 100;
		if(c <= 60 || m.isEmpty()) put(m,rand);
		else remove(m,rand);
	}
	
	private static void put(Map<Integer,Integer> m, Random rand) {
		m.put(rand.nextInt(maxValue),rand.nextInt(maxValue));
	}
	
	private static void remove(Map<Integer,Integer> m, Random rand) {
		m.remove(rand.nextInt(maxValue));
	}
	
	@SuppressWarnings("deprecation")
	private static <K,V> void printBucketStatistics(ring.collections.impl.HashMap<K,V> m) {
		var data = m.bucketStatistics();
		var b = new StringBuilder();
		for(var e : data) {
			if(b.length() > 0) b.append(", ");
			b.append(e);
		}
		System.out.println("bucket statistics: "+b);
	}
}
