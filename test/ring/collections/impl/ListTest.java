package ring.collections.impl;
/* Copyright 2020 Lukas Habring
 * 
 * For commercial licenses, just contact Lukas Habring (see README for details).
 * This file is part of Ring.Collections.
 * 
 * Ring.Collections is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * (and only version 3) as published by the Free Software Foundation.
 * 
 * Ring.Collections is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Ring.Collections.  If not, see <http://www.gnu.org/licenses/>.
 * */

import java.util.Iterator;
import java.util.Random;
import java.util.function.BiConsumer;

import ring.jcollections.BatchArrayList;
import ring.jcollections.GapArrayList;
import ring.jcollections.JWrapper;

public class ListTest {
	static final ErrorMessages errors = new ErrorMessages();
	static final int testRuns = 100000, benchmarkFactor = 10;
	static final int maxValue = 1000, maxSize = 10000, seed = 0;
	
	public static void main(String[] args) {
		test(new GapArrayList<>(),testRuns,testRuns*benchmarkFactor);
		test(new BatchArrayList<>(),testRuns,testRuns*benchmarkFactor);
		test(JWrapper.wrap(new ring.collections.impl.ArrayList<>()),testRuns,testRuns);
		benchmark(new java.util.ArrayList<>(),ListTest::randomAction,testRuns*benchmarkFactor);
	}
	
	private static void test(java.util.List<Integer> l, int testRuns, int benchmarkRuns) {
		randomTest(l,new java.util.ArrayList<Integer>(),testRuns);
		l.clear();
		if(l.size() != 0) throw new Error(errors.unreachable);
		benchmark(l,ListTest::randomAction,benchmarkRuns);
	}
	
	private static void randomTest(java.util.List<Integer> l1, java.util.List<Integer> l2, int runs) {
		var rand1 = new Random(seed); var rand2 = new Random(seed);
		for(int i = 0; i < runs; i++) {
			randomAction(l1,rand1); randomAction(l2,rand2);
			if(!l1.equals(l2)) {
				System.out.printf("run: %d\n%s\n%s\n",i,l1,l2);
				throw new Error(errors.unreachable);
			}
		}
	}
	
	static <T> void benchmark(T obj, BiConsumer<T,Random> randomAction, int runs) {
		var rand = new Random(seed);
		var t = System.nanoTime();
		for(int i = 0; i < runs; i++) randomAction.accept(obj,rand);
		t = System.nanoTime() - t;
		final int benchmarkRuns = testRuns * benchmarkFactor;
		t = Math.round(t * (benchmarkRuns / (double)runs));
		System.out.println(obj.getClass().getName()+":");
		System.out.printf("runtime: %.2f ms\n",t/1000000D);
		System.out.printf("%.2f ns/run\n",t/(double)benchmarkRuns);
		System.out.printf("%.2f runs/ms\n\n",benchmarkRuns*1000000D/t);
	}
	
	private static void randomAction(java.util.List<Integer> l, Random rand) {
		if(l.size() >= maxSize) l.clear();
		var c = rand.nextDouble() * 100;
		if(c <= 30 || l.isEmpty()) add(l,true,rand);
		else if(c <= 60) remove(l,rand);
		else if(c <= 80) set(l,rand);
		else if(c <= 90) addMulti(l,rand);
		else removeMulti(l,rand);
	}
	
	private static int randSize(Random rand) { return Math.round((float)Math.abs(rand.nextGaussian() * 1000)); }
	
	private static void set(java.util.List<Integer> l, Random rand) { l.set(rand.nextInt(l.size()),rand.nextInt(maxValue)); }
	
	private static void add(java.util.List<Integer> l, boolean randIdx, Random rand) {
		int i = randIdx ? rand.nextInt(l.size()+1) : l.size();
		l.add(i,rand.nextInt(maxValue));
	}
	
	private static void remove(java.util.List<Integer> l, Random rand) { l.remove(rand.nextInt(l.size())); }
	
	private static void addMulti(java.util.List<Integer> l, Random rand) {
		var elems = new java.util.AbstractCollection<Integer>() {
			final int size1 = randSize(rand), size2 = rand.nextInt(10) == 0 ? size1+rand.nextInt(10)-5 : size1;
			final long seed = rand.nextLong();
			@Override public int size() { return size1; }
			@Override
			public Iterator<Integer> iterator() {
				var rand = new Random(seed);
				return new Iterator<>() {
					int nextIdx = 0;
					@Override public boolean hasNext() { return nextIdx < size2; }
					@Override public Integer next() { nextIdx++; return rand.nextInt(maxValue); }
				};
			}
		};
		l.addAll(rand.nextInt(l.size()+1),elems);
	}
	
	private static void removeMulti(java.util.List<Integer> l, Random rand) {
		var m = rand.nextInt();
		l.removeIf(e->(Integer.bitCount(e*m)&1) == 0);
	}
}
